#include <QApplication>
#include "main_widget.h"


int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    {
        auto w = new MainWidget;
        w->show();
        a.exec();
        delete w;
    }
    printUsageMemory();
    return 0;
}

