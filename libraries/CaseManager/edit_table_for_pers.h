#ifndef EDIT_TABLE_FOR_PERS_H
#define EDIT_TABLE_FOR_PERS_H
#include "person_edit.h"
#include "factory_add_anything.h"
#include "central_widget.h"
#include "edit_table_for_rec.h"


class Edit_table_for_pers: public PersonFileEditor, public CentralWidget
{
    using CreationCase = AddingCustomWidgets<QLineEdit*>;
public:
    Edit_table_for_pers(QString directory, QWidget* parent = 0);
    QString getCurrentPath();
    void updateCaseView();
    void allPersLoad();
    void addPers();
    void rePers();
    void copyPers();
    void delPers();
    void exchangeDir();
    void exchangeFileDir();
    void initButton(QPushButton* button, const QString& style, int PointSize);
private:
    std::unique_ptr<CreationCase> addPersonView;
    std::unique_ptr<Edit_table_for_rec> recTable;
    QVBoxLayout mainVLayout;
    QLabel sectionTitle;
    QHBoxLayout hLayForCaseButtons;
    QPushButton bAddCase;
    QPushButton bRenameCase;
    QPushButton bChangeCaseDir;
    QPushButton bCopyInfoCase;
    QPushButton bDeleteCase;
    QHBoxLayout layoutForTables;
    std::unique_ptr<QTableWidget> caseTable;
    QPushButton bExchangeDir;
    QString otherDirectoryName;
    QString defaultСategoryName;
};

#endif // EDIT_TABLE_FOR_PERS_H
