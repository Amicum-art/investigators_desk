#ifndef EDITWIDGETFORREC_H
#define EDITWIDGETFORREC_H
#include "record_edit.h"
#include "factory_add_anything.h"



class EditWidgetForRec :
        public RecordFileEditor,
        public AddingCustomWidgets <QComboBox*, QDateTimeEdit*, QLineEdit*>
{
public:
    explicit EditWidgetForRec
            (QComboBox* box = new QComboBox(),
            QDateTimeEdit* DateTime = new QDateTimeEdit(),
            QLineEdit* line = new QLineEdit());
};

#endif // EDITWIDGETFORREC_H
