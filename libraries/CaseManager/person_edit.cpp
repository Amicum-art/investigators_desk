#include "person_edit.h"
#include "crypt.h"


void PersonFileEditor::addPerson(const QString &_fileDir,
                                 const QString &_fileName)
{
    if(_fileName.isEmpty()) return;
    if (!QDir(_fileDir).exists())
        QDir().mkdir(_fileDir);
    auto fileName = Code::crypt(QStringRef(&_fileName));
    QString currFilePath = _fileDir + "/" + fileName + ".txt";
    QFile file(currFilePath);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append))
        file.close();
}

void PersonFileEditor::renamePerson(const QString &fileDir,
                                    const QString &oldName,
                                    const QString &newName)
{
    if(newName.isEmpty()) return;
    auto fileName = fileDir + '/' + Code::crypt(QStringRef(&oldName));
    auto currentFilePath = fileDir + '/' + fileName + ".txt";
    QFile file(currentFilePath);
    if(file.exists())
        file.rename(fileDir + '/' + Code::crypt(QStringRef(&newName)) + ".txt");
}

void PersonFileEditor::replasePerson(const QString &oldDir,
                                     const QString &NewDir,
                                     const QString &fileName)
{
    auto cryptFileName = Code::crypt(QStringRef(&fileName));
    auto oldFilePath = oldDir + '/' + cryptFileName + ".txt";
    QString nextFilePath = NewDir + '/';
    if (!QDir(nextFilePath).exists()) QDir().mkdir(nextFilePath);
    QFile::copy(oldFilePath, nextFilePath + cryptFileName + ".txt");
    QFile::remove(oldFilePath);
}

void PersonFileEditor::deletePerson(const QString &fileDir,
                                    const QString &fileName)
{
    auto cryptFileName = Code::crypt(QStringRef(&fileName));
    auto filePath = fileDir + '/' + cryptFileName + ".txt";
    if(QFile(filePath).exists())
        QFile::remove(filePath);
}



std::optional<QString> PersonFileEditor::findDir(const QString &fileName)
{
    if(fileName.isEmpty()) return {};
    for (const QString &directory: {"Текущие уголовные дела", "Архивные уголовные дела",
        "Текущие материалы проверки", "Архивные материалы проверки"})
    {
        if (QFileInfo(directory).isDir())
        {
            QDir dir(directory);
            dir.setNameFilters(QStringList("*.txt"));
            dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
            QStringList fileList = dir.entryList();
            QString finderFile = Code::crypt((QString) fileName);
            for (const auto &file: fileList)
            {
                if (QStringRef(&file, 0, file.size() - 4) == finderFile)
                {
                    return directory;
                }
            }
        }
    }
    return {};
}

std::optional<QStringList> PersonFileEditor::getCasesList()
{
    QStringList result{};
    for (const QString &widget: {"Текущие уголовные дела", "Архивные уголовные дела",
         "Текущие материалы проверки", "Архивные материалы проверки"})
    {
        if (QFileInfo(widget).isDir())
        {
            QDir dir(widget);
            dir.setNameFilters({"*.txt"});
            dir.setFilter(QDir::Files |
                          QDir::NoDotAndDotDot |
                          QDir::NoSymLinks);
            QStringList fileList = dir.entryList();
            for (const auto &file: fileList)
            {
                QStringRef ref (&file, 0, file.length() - 4);
                result.emplace_back(Code::deCrypt(ref));
            }
        }
    }
    return result;
}
