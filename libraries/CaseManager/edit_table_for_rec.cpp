#include "edit_table_for_rec.h"


Edit_table_for_rec::Edit_table_for_rec(QString filePath,
                                       QWidget* parent)
        : QWidget(parent), currentCase (filePath),
        vLayMain (QVBoxLayout(this)),
        hLayForRecordButtons(QHBoxLayout()),
        bAddRecord(QPushButton("Новая запись", this)),
        bRenameRecord(QPushButton("Изменить запись", this)),
        bDeleteRecord(QPushButton("Удалить запись", this)),
        caseTable(std::make_unique<QTableWidget>(0, 1, this))
{
    caseTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    caseTable->horizontalHeader()->setHidden(true);
    caseTable->verticalHeader()->setHidden(true);
    caseTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    caseTable->setStyleSheet("selection-color: rgba(0, 0, 0, 170);"
                            "font: 9pt;"
                            "selection-background-color: rgba(240, 240, 240, 1);");
    caseTable->setFrameStyle(QFrame::NoFrame);
    caseTable->verticalHeader()->setStyleSheet("color: rgb(10,101,10);");
    caseTable->setSelectionMode(QAbstractItemView::ExtendedSelection);///////////
    caseTable->setFocusPolicy(Qt::NoFocus);
    caseTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    vLayMain.addWidget(caseTable.get());
    initButton(&bAddRecord, "background-color: rgba(108,108,108,0.5);", 8);
    hLayForRecordButtons.addWidget(&bAddRecord);
    initButton(&bRenameRecord, "background-color: rgba(108,108,108,0.5);", 8);
    hLayForRecordButtons.addWidget(&bRenameRecord);
    initButton(&bDeleteRecord, "background-color: rgba(108,108,108,0.5);", 8);
    hLayForRecordButtons.addWidget(&bDeleteRecord);
    vLayMain.addLayout(&hLayForRecordButtons);
    setLayout(&vLayMain);
    updateCaseView();
    connect(&bAddRecord, &QPushButton::clicked, this, &Edit_table_for_rec::addRecord);
    connect(&bRenameRecord, &QPushButton::clicked, this,  &Edit_table_for_rec::reRecord);
    connect(&bDeleteRecord, &QPushButton::clicked, this, &Edit_table_for_rec::delRecord);
    connect(this, &Edit_table_for_rec::onChangeFile, this, &Edit_table_for_rec::changeCase);
    connect(caseTable.get(), &QTableWidget::currentItemChanged, this, [this](){
        static const QString style100 = "background-color: rgba(108,108,108,1);";
        bRenameRecord.setEnabled(true);
        bRenameRecord.setStyleSheet(style100);
        bDeleteRecord.setEnabled(true);
        bDeleteRecord.setStyleSheet(style100);
    });
}

QString Edit_table_for_rec::getDateFromRec(const QString &record)
{
    auto isFullTime = record[13] == ':';
    return record.first(isFullTime? 16 : 15);
}

void Edit_table_for_rec::initButton(QPushButton *button,
                                    const QString &style,
                                    int PointSize)
{
    button->setFont(QFont("TimesNewRoman", PointSize));
    button->setStyleSheet(style);
    button->setCursor(QCursor(Qt::PointingHandCursor));
}

void Edit_table_for_rec::addRecord()
{
    auto time = new QDateTimeEdit(QDateTime::currentDateTime());
    auto lineEdit = new QLineEdit();
    wForAddRecord = std::make_unique<CreationRecord>(time, lineEdit);
    wForAddRecord->setWindowTitle("Добавление новой записи");
    connect(wForAddRecord.get(), &CreationRecord::accepted,
            this, [this, time, lineEdit]()
    {
        if(auto error = RecordFileEditor::addRecord({time->text(), lineEdit->text()}, currentCase);
                auto text = std::get_if<QString>(&error))
            showMessage(*text);
        else
            updateCaseView();
    });
    wForAddRecord->show();
}

void Edit_table_for_rec::reRecord()
{
    if(!caseTable->currentItem())
        return;
    auto time = new QDateTimeEdit(QDateTime::currentDateTime());
    auto lineEdit = new QLineEdit;
    wForAddRecord = std::make_unique<CreationRecord>(time, lineEdit);
    wForAddRecord->setWindowTitle("Изменение записи");
    connect(wForAddRecord.get(), &CreationRecord::accepted,
            this, [this, time, lineEdit]()
    {
        if(time->text().isEmpty() || lineEdit->text().isEmpty())
            return;
        auto vec = getCaseRecords(currentCase);
        auto item = caseTable->currentItem();
        const auto & itemDate = getDateFromRec(item->text());
        for (auto &it: vec)
        {
            if (itemDate == it.dateTime)
            {
                it.note = lineEdit->text();
                it.dateTime = time->text();
                break;
            }
        }
        saveRecordsToFile(currentCase, vec);
        updateCaseView();
    });
    wForAddRecord->show();
}

void Edit_table_for_rec::delRecord()
{
    if(!caseTable->currentItem())
        return;
    auto dateTime = getDateFromRec(caseTable->currentItem()->text());
    if(RecordFileEditor::deleteRecord(dateTime, currentCase))
        updateCaseView();
}

void Edit_table_for_rec::updateCaseView()
{
    static const QString style50 = "background-color: rgba(108,108,108, 0.5);";
    caseTable->setRowCount(0);
    if(QFile(currentCase).exists()){
        auto vec = getCaseRecords(currentCase);
        for (const auto &it: vec) {
            caseTable->setRowCount(caseTable->rowCount()+1);
            auto ptr = new QTableWidgetItem(it.dateTime + " - " + it.note);
            ptr->setForeground(QBrush(Qt::black));
            caseTable->setItem(caseTable->rowCount() - 1, 0, ptr);
        }
        bAddRecord.setStyleSheet("background-color: rgba(108,108,108,1);");
        bAddRecord.setEnabled(true);
        if(!caseTable->currentItem()){
            bRenameRecord.setEnabled(false);
            bRenameRecord.setStyleSheet(style50);
            bDeleteRecord.setEnabled(false);
            bDeleteRecord.setStyleSheet(style50);
        }
    } else {
        bAddRecord.setStyleSheet(style50);
        bAddRecord.setEnabled(false);
        bRenameRecord.setEnabled(false);
        bRenameRecord.setStyleSheet(style50);
        bDeleteRecord.setEnabled(false);
        bDeleteRecord.setStyleSheet(style50);
    }
    show();
}


void Edit_table_for_rec::changeCase(QString filePath)
{
    currentCase = filePath;
    updateCaseView();
}

void Edit_table_for_rec::showMessage(QString messageText)
{
    auto box = new QMessageBox(this);
    box->setStyleSheet("background-color: rgba(220,220,215,0.9);\
                        color: black;");
    box->setFixedHeight(box->hasHeightForWidth());
    box->setAttribute(Qt::WA_DeleteOnClose);
    box->setWindowTitle("В это время у вас");
    box->setText(messageText);
    box->show();
}
