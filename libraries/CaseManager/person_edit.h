#ifndef PERSONEDIT_H
#define PERSONEDIT_H


class PersonFileEditor
{
public:
    PersonFileEditor() = default;
    virtual ~PersonFileEditor() = default;
    void addPerson
            (const QString& fileDir,
             const QString& fileName);
    void renamePerson
            (const QString& fileDir,
             const QString& oldName,
             const QString& newName);
    void replasePerson
            (const QString& oldDir,
             const QString& NewDir,
             const QString& fileName);
    void deletePerson
            (const QString& fileDir,
             const QString& fileName);
    static std::optional<QString> findDir
            (const QString& fileName);
    static std::optional<QStringList> getCasesList();
};

#endif // PERSONEDIT_H
