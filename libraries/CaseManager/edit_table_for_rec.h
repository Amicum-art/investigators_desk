#ifndef EDIT_TABLE_FOR_REC_H
#define EDIT_TABLE_FOR_REC_H
#include "factory_add_anything.h"
#include "record_edit.h"


class Edit_table_for_rec: public QWidget, public RecordFileEditor
{
    Q_OBJECT
    using CreationRecord = AddingCustomWidgets<QDateTimeEdit*, QLineEdit*>;
public:
    Edit_table_for_rec(QString filePath, QWidget* parent = 0);
    static QString getDateFromRec(const QString& record);
    void addRecord();
    void reRecord();
    void delRecord();
private:
    void initButton(QPushButton *button,
                            const QString &style,
                            int PointSize);
    void updateCaseView();
    QString currentCase;
    QVBoxLayout vLayMain;
    QHBoxLayout hLayForRecordButtons;
    QPushButton bAddRecord;
    QPushButton bRenameRecord;
    QPushButton bDeleteRecord;
    std::unique_ptr<QTableWidget> caseTable;
    std::unique_ptr<CreationRecord> wForAddRecord;
signals:
    void onChangeFile(QString filePath);
public slots:
    void changeCase(QString filePath);
    void showMessage(QString messageText);
};


#endif // EDIT_TABLE_FOR_REC_H
