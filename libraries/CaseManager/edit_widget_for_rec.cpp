#include "edit_widget_for_rec.h"
#include "person_edit.h"
#include "crypt.h"

EditWidgetForRec::EditWidgetForRec(QComboBox* box, QDateTimeEdit* DateTime, QLineEdit* line) :
    AddingCustomWidgets<QComboBox*, QDateTimeEdit*, QLineEdit*>(box, DateTime, line)
{
    auto screen = QGuiApplication::screens().constFirst()->availableGeometry();
    resize(screen.width() * .476, screen.height() * .139);
    setWindowTitle("");
    setStyleSheet(QString::fromUtf8("background-color: rgb(238, 233, 228);"));
    box->setMinimumWidth(screen.width() * .146);
    box->setEditable(true);
    box->lineEdit()->setReadOnly(true);
    box->lineEdit()->setAlignment(Qt::AlignHCenter);
    box->setAutoFillBackground(true);
    box->setCursor(QCursor(Qt::PointingHandCursor));
    if(auto actualCases = PersonFileEditor::getCasesList(); actualCases.has_value()){
        for (const auto &it: actualCases.value()){
            box->addItem(it);
        }
    }
    DateTime->setDateTime(QDateTime::currentDateTime());
    auto *listView = new QListView(DateTime);
    listView->setStyleSheet(
        "QListView::item {                          \
            background-color: rgb(235,235,235);     \
            color: black;                           \
        }                                           \
        QListView::item:selected {                  \
            background-color: rgba(220,220,215,0.9);\
            color: black;                           \
        }");
    box->setView(listView);
    connect(this, &QDialog::accepted, this, [box, DateTime, line]()
    {
        if (box->currentIndex() != -1 && !line->text().isEmpty())
        {
            auto fileName = box->currentText();
            auto cryptName = Code::crypt(fileName) + ".txt";
            if(auto cryptDir = PersonFileEditor::findDir(fileName); cryptDir.has_value())
            {
                auto filePath = cryptDir.value() + '/' + cryptName;
                addRecord({DateTime->text(), line->text()}, filePath);
            }
        }
    });
}
