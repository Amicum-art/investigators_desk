#include "edit_table_for_pers.h"
#include "crypt.h"

Edit_table_for_pers::Edit_table_for_pers(QString directory,
                                        QWidget *parent):
    CentralWidget(parent),
    recTable(std::make_unique<Edit_table_for_rec>("", this)),
    mainVLayout(QVBoxLayout(this)),
    sectionTitle(QLabel(this)),
    hLayForCaseButtons(QHBoxLayout()),
    bAddCase(QPushButton("Добавить", this)),
    bRenameCase(QPushButton("Переименовать", this)),
    bChangeCaseDir(QPushButton("Переместить в архивные", this)),
    bCopyInfoCase(QPushButton("Копировать", this)),
    bDeleteCase(QPushButton("Удалить", this)),
    layoutForTables(QHBoxLayout()),
    caseTable(std::make_unique<QTableWidget>(0, 1, this)),
    bExchangeDir(QPushButton(this)),
    otherDirectoryName("Архивные " + directory),
    defaultСategoryName("Текущие " + directory)
{
    setStyleSheet("background-color: rgba(255,255,255,0.1)");
    sectionTitle.setStyleSheet("selection-color: rgba(99, 99, 99, 170);"
                         "color: rgb(0,0,0);"
                         "font: 75 12pt;"
                         "selection-background-color: rgba(213, 213, 213, 179);");
    sectionTitle.setText(defaultСategoryName);
    sectionTitle.setAlignment(Qt::AlignmentFlag::AlignCenter);
    mainVLayout.addWidget(&sectionTitle);
    initButton(&bAddCase, "background-color: rgba(108,108,108,1);", 10);
    hLayForCaseButtons.addWidget(&bAddCase);
    initButton(&bRenameCase, "background-color: rgba(108,108,108,0.5);", 10);
    bRenameCase.setEnabled(false);
    hLayForCaseButtons.addWidget(&bRenameCase);
    initButton(&bChangeCaseDir, "background-color: rgba(108,108,108,0.5);", 10);
    bChangeCaseDir.setEnabled(false);
    hLayForCaseButtons.addWidget(&bChangeCaseDir);
    initButton(&bCopyInfoCase, "background-color: rgba(108,108,108,0.5);", 10);
    bCopyInfoCase.setEnabled(false);
    hLayForCaseButtons.addWidget(&bCopyInfoCase);
    initButton(&bDeleteCase, "background-color: rgba(108,108,108,0.5);", 10);
    bDeleteCase.setEnabled(false);
    hLayForCaseButtons.addWidget(&bDeleteCase);
    mainVLayout.addLayout(&hLayForCaseButtons);
    caseTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    caseTable->horizontalHeader()->setHidden(true);
    caseTable->setStyleSheet("selection-color: rgba(0, 0, 0, 170);"
                            "font: 9pt;"
                            "selection-background-color: rgba(240, 240, 240, 1);");
    caseTable->setFrameStyle(QFrame::NoFrame);
    caseTable->setFixedWidth(120);
    caseTable->verticalHeader()->setStyleSheet("color: rgb(10,101,10);");
    caseTable->setSelectionMode(QAbstractItemView::ExtendedSelection);///////////
    caseTable->setFocusPolicy(Qt::NoFocus);
    caseTable->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    layoutForTables.setAlignment(Qt::AlignmentFlag::AlignLeft);
    layoutForTables.addWidget(caseTable.get());
    layoutForTables.addWidget(recTable.get());
    initButton(&bExchangeDir, "font: 12pt;"
                             "background-color: rgba(108,108,108,1);", 12);
    bExchangeDir.setText(otherDirectoryName);
    mainVLayout.addLayout(&layoutForTables);
    mainVLayout.addWidget(&bExchangeDir);
    setLayout(&mainVLayout);
    connect(&bAddCase, &QPushButton::clicked, this, &Edit_table_for_pers::addPers);
    connect(&bRenameCase, &QPushButton::clicked, this, &Edit_table_for_pers::rePers);
    connect(&bChangeCaseDir, &QPushButton::clicked, this, &Edit_table_for_pers::exchangeFileDir);
    connect(&bDeleteCase, &QPushButton::clicked, this, &Edit_table_for_pers::delPers);
    connect(&bCopyInfoCase, &QPushButton::clicked, this, &Edit_table_for_pers::copyPers);
    connect(&bExchangeDir, &QPushButton::clicked, this, &Edit_table_for_pers::exchangeDir);
    connect(caseTable.get(), &QTableWidget::currentCellChanged, this, &Edit_table_for_pers::updateCaseView);
    allPersLoad();
}

void Edit_table_for_pers::updateCaseView()
{
    static const QString style100 = "background-color: rgba(108,108,108,1);";
    static const QString style50 = "background-color: rgba(108,108,108,5);";
    if (caseTable->currentItem()) {
        emit recTable->onChangeFile(getCurrentPath());
        if(recTable->isHidden()) recTable->show();
        bRenameCase.setEnabled(true);
        bRenameCase.setStyleSheet(style100);
        bChangeCaseDir.setEnabled(true);
        bChangeCaseDir.setStyleSheet(style100);
        bCopyInfoCase.setEnabled(true);
        bCopyInfoCase.setStyleSheet(style100);
        bDeleteCase.setEnabled(true);
        bDeleteCase.setStyleSheet(style100);
    } else {
        if(recTable->isVisible()) recTable->hide();
        bRenameCase.setEnabled(false);
        bRenameCase.setStyleSheet(style50);
        bChangeCaseDir.setEnabled(false);
        bChangeCaseDir.setStyleSheet(style50);
        bCopyInfoCase.setEnabled(false);
        bCopyInfoCase.setStyleSheet(style50);
        bDeleteCase.setEnabled(false);
        bDeleteCase .setStyleSheet(style50);
    }

}

void Edit_table_for_pers::allPersLoad()
{
    caseTable->setRowCount(0);
    if (QFileInfo(defaultСategoryName).isDir())
    {
        QDir dir(defaultСategoryName);
        dir.setNameFilters(QStringList("*.txt"));
        dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        QStringList fileList = dir.entryList();
        for (const auto &file: fileList)
        {
            QString caseName = Code::deCrypt(QStringRef(&file, 0, file.length() - 4));
            caseTable->setRowCount(caseTable->rowCount()+1);
            auto ptrCase = new QTableWidgetItem(caseName);
            ptrCase->setForeground(QBrush(Qt::black));
            caseTable->setItem(caseTable->rowCount() - 1, 0, ptrCase);
        }
        caseTable->resizeRowsToContents();
        caseTable->show();
    } else QDir().mkdir(defaultСategoryName);
}

void Edit_table_for_pers::addPers()
{
    auto lineEdit = new QLineEdit();
    addPersonView = std::make_unique<CreationCase>(lineEdit);
    addPersonView->setWindowTitle("Добавление нового дела");
    connect(addPersonView.get(), &CreationCase::accepted,
            this, [this, lineEdit]()
    {
        addPerson(defaultСategoryName, lineEdit->text());
        allPersLoad();
    });
    addPersonView->show();
}

void Edit_table_for_pers::rePers()
{
    if (caseTable->currentItem())
    {
        auto lineEdit = new QLineEdit();
        addPersonView = std::make_unique<CreationCase>(lineEdit);
        addPersonView->setWindowTitle("Изменение названия");
        connect(addPersonView.get(), &CreationCase::accepted,
                this, [this, lineEdit]()
        {
            renamePerson(defaultСategoryName,
                        caseTable->currentItem()->text(),
                        lineEdit->text());
            addPerson(defaultСategoryName, lineEdit->text());
            allPersLoad();
        });
        addPersonView->show();
    }
}

void Edit_table_for_pers::copyPers()
{
    if (caseTable->currentItem())
    {
        recTable->copyRecords(getCurrentPath());
    }
}

void Edit_table_for_pers::delPers()
{
    if (caseTable->currentItem()) {
        deletePerson(defaultСategoryName,caseTable->currentItem()->text());
        allPersLoad();
        ////
    }
}


void Edit_table_for_pers::exchangeFileDir()
{
    if (auto person = caseTable->currentItem())
    {
        replasePerson(defaultСategoryName, otherDirectoryName, person->text());
        allPersLoad();
    }
}

QString Edit_table_for_pers::getCurrentPath()
{
    if(auto item = caseTable->currentItem())
        return defaultСategoryName + '/' + Code::crypt(item->text()) + ".txt";
    else return {};
}

void Edit_table_for_pers::exchangeDir()
{
    std::swap(defaultСategoryName, otherDirectoryName);
    sectionTitle.setText(defaultСategoryName);
    bExchangeDir.setText(otherDirectoryName);
    bChangeCaseDir.setText("Переместить в " +
                            otherDirectoryName.first(8));
    allPersLoad();
}



void Edit_table_for_pers::initButton(QPushButton *button,
                                    const QString &style,
                                    int PointSize)
{
    button->setFont(QFont("TimesNewRoman", PointSize));
    button->setStyleSheet(style);
    button->setCursor(QCursor(Qt::PointingHandCursor));
}
