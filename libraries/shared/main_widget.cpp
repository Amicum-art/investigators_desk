#include "main_widget.h"
#include "default_scene.h"
#include "qtoolbar.h"
#include "edit_table_for_pers.h"
#include "inventory_scene.h"
#include "crypt.h"

void* operator new (size_t size){
    //qDebug() << __LINE__ << "Allocated size" << size;
    s_AllocationMetrics.TotalAllocated+= size;
    return malloc(size);
}

void operator delete (void* memory, size_t size) noexcept
{
    //qDebug() << __LINE__ <<  "Freed size" << size;
    s_AllocationMetrics.TotalFreed += size;
    return free(memory);
}

void operator delete(void* memory) noexcept
{
    s_AllocationMetrics.TotalFreed += sizeof(memory);
    free(memory);
}

MainWidget::MainWidget() :
    toolbar(new QToolBar(this)),
    scene(new DefaultScene(this)),
    auth(new Authorization(this))
{
    Code::initMap();
    toolbar->hide();
    scene->hide();
    setCentralWidget(auth);
    connect(auth, &Authorization::onLogin, this, [this](QString username,
                                                        QString password){
        Code::key = username + password;
        toolbar->show();
        scene->show();
        setCentralWidget(scene);
        addToolBar(Qt::LeftToolBarArea, toolbar);
        setWindowIcon(QIcon("qss/main.jpg"));
        toolbar->setFloatable(false);
        toolbar->setMovable(false);
        toolbar->setOrientation(Qt::Vertical);
        toolbar->setAcceptDrops(false);
        toolbar->move(geometry().width() - toolbar->geometry().width(), 0);
        toolbar->setContextMenuPolicy(Qt::PreventContextMenu);
        auto widget = initMenuButton("Конвертер", "qss/default/load.png");
        connect(widget, &QPushButton::clicked, this, [this](){
            changeCentralW(new DefaultScene(this));
        } );
        widget = initMenuButton("Уголовные дела", "qss/default/D.png");
        connect(widget, &QPushButton::clicked, this, [this](){
            changeCentralW(new Edit_table_for_pers("уголовные дела",this));
        } );
        widget = initMenuButton("Материалы проверки", "qss/default/M.png");
        connect(widget, &QPushButton::clicked, this, [this](){
            changeCentralW(new Edit_table_for_pers("материалы проверки",this));
        } );
        widget = initMenuButton("Опись", "qss/default/W.png");
        connect(widget, &QPushButton::clicked, this, [this](){
            changeCentralW(new Inventory_scene(this));
        } );
        delete auth;
    } );
}

QPushButton *MainWidget::initMenuButton(QString toolTip, QString iconPath)
{
    auto button = new QPushButton(this);
    button->resize(25,25);
    button->setCursor(QCursor(Qt::PointingHandCursor));
    button->setIconSize(QSize(25, 25));
    button->setToolTip(toolTip);
    button->setIcon(QIcon(iconPath));
    toolbar->addWidget(button);
    toolbar->addSeparator();
    widgets.append(button);
    return button;
}

void MainWidget::changeCentralW(CentralWidget *wgt)
{
    delete scene;
    scene = wgt;
    setCentralWidget(scene);
}

