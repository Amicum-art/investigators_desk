#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H


class Authorization : public QWidget {
    Q_OBJECT
    QFrame *frame;
    QLabel *label;
    QPushButton *pushButton;
    QLineEdit *login;
    QLineEdit *password;
    QToolButton *toolButton_2;
public:
    Authorization(QWidget* parent);
signals:
    void onLogin(QString username,
                 QString password);
public slots:
    void onPressButtonLogin();
};

#endif // AUTHORIZATION_H
