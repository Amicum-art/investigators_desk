#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "central_widget.h"
#include <authorization.h>
#ifndef OPN
#define OPN

struct AllocationMetrics {
    uint32_t TotalAllocated = 0;
    uint32_t TotalFreed = 0;
    uint32_t CurrentUsage(){ return TotalAllocated - TotalFreed; }
};

static AllocationMetrics s_AllocationMetrics;

[[maybe_unused]] static void printUsageMemory(){
    qDebug() << "Memory Usage:" << s_AllocationMetrics.CurrentUsage();
}

#endif


class MainWidget : public QMainWindow
{
public:
    MainWidget();
private:
    QPushButton* initMenuButton(QString toolTip, QString iconPath);
    void changeCentralW(CentralWidget* wgt);
private:
    QList<QPushButton *> widgets;
    QToolBar *toolbar;
    CentralWidget* scene;
    Authorization* auth;
};


#endif // MAINWIDGET_H
