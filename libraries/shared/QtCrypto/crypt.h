#ifndef CRYPT_H
#define CRYPT_H
#include "qaesencryption.h"
#include <QStringRef>
#include <QCryptographicHash>

static QAESEncryption encryption(QAESEncryption::AES_256, QAESEncryption::ECB, QAESEncryption::Padding::ISO);



class Code {
    static std::unordered_map<QString, QString> map;
public:
    static void initMap();
    static QString key;
    template<typename T,
             typename = std::enable_if_t<std::is_same_v<QString, std::remove_cv_t<std::remove_reference_t<T>>>
                                         || std::is_same_v<QStringRef, std::remove_cv_t<std::remove_reference_t<T>>>>>
    static QString crypt(T&& inputStr) {
        QByteArray hashKey = QCryptographicHash::hash(key.toLocal8Bit(), QCryptographicHash::Sha256);
        QString encodeText = encryption.encode(inputStr.toUtf8(), hashKey).toHex();
        QString temp{};
        for (const auto &it: encodeText) {
            if (map.find(it) != map.end()) {
                temp += map[it];
            } else {
                temp += it + QString::fromStdString("0");
            }
        }
        return temp;
    }

    template<typename T,
             typename = std::enable_if_t<std::is_same_v<QString, std::remove_cv_t<std::remove_reference_t<T>>>
                                         || std::is_same_v<QStringRef, std::remove_cv_t<std::remove_reference_t<T>>>>>
    static QString deCrypt(T&& encodeText) {
        QString tmp{};
        QString res{};
        for (const auto &it: encodeText) {
            if (!(tmp.length() % 2))
                tmp = "";
            tmp += it;
            if (tmp.length() == 2) {
                bool find = false;
                for (const auto &itr: map) {
                    if (itr.second == tmp) {
                        res += itr.first;
                        find = true;
                    }
                }
                if (!find) res += tmp[0];
            }
        }

        QByteArray hashKey = QCryptographicHash::hash(key.toLocal8Bit(), QCryptographicHash::Sha256);
        auto result =  encryption.removePadding(encryption.decode(QByteArray::fromHex(res.toUtf8()), hashKey));
        return result.toStdString().c_str();
    }
};

#endif // CRYPT_H
