#include "authorization.h"

Authorization::Authorization(QWidget* parent)
    :QWidget(parent)
{
    setStyleSheet(QString::fromUtf8(
            "*{\n"
            "	font-family: century gothic;\n"
            "	font-size: 24px;\n"
            "}\n"
            "QFrame{\n"
            "	background: rgba(0,0,0,0.8); \n"
            "	border-radius: 15px;\n"
            "}\n"
            "QToolButton{\n"
            "	background: red;\n"
            "	border-radius: 60px;\n"
            "}\n"
            "QLabel{\n"
            "	color: white;\n"
            "	background: transparent;\n"
            "}\n"
            "QPushButton{\n"
            "	color: white; \n"
            "	background: red;\n"
            "	border-radius: 15px;	\n"
            "} \n"
            "QPushButton:hover{\n"
            "	color: #333; \n"
            "	border-radius: 15px;	\n"
            "	background:  green;\n"
            "} \n"
            "QLineEdit{\n"
            "	background: transparent;\n"
            "	border: none;\n"
            "	color: #717072;\n"
            "	border-bottom: 1px solid #717072;\n"
            "}"));
    setFixedSize(588,410);
    frame = new QFrame(this);
    frame->setGeometry(QRect(105, 60, 381, 320));
    frame->setFrameShape(QFrame::StyledPanel);
    frame->setFrameShadow(QFrame::Raised);
    label = new QLabel(frame);
    label->setGeometry(QRect(110, 60, 171, 41));
    pushButton = new QPushButton(frame);
    pushButton->setGeometry(QRect(80, 250, 211, 41));
    pushButton->setShortcut(Qt::Key_Enter);
    pushButton->setShortcut(Qt::Key_Return);
    login = new QLineEdit(frame);
    login->setGeometry(QRect(40, 120, 291, 31));
    password = new QLineEdit(frame);
    password->setGeometry(QRect(40, 190, 291, 31));
    password->setEchoMode(QLineEdit::Password); 
    toolButton_2 = new QToolButton(this);
    toolButton_2->setGeometry(QRect(235, 0, 121, 121));
    QIcon icon;
    icon.addFile(QString::fromUtf8("qss/png/login.png"), QSize(), QIcon::Normal, QIcon::Off);
    toolButton_2->setIcon(icon);
    toolButton_2->setIconSize(QSize(64, 128));
    label->setText(QCoreApplication::translate("MainWindow", "Authorization", nullptr));
    pushButton->setText(QCoreApplication::translate("MainWindow", "LOGIN", nullptr));
    login->setText(QString());
    login->setPlaceholderText(QCoreApplication::translate("MainWindow", "Username", nullptr));
    password->setText(QString());
    password->setPlaceholderText(QCoreApplication::translate("MainWindow", "Password", nullptr));
    toolButton_2->setText(QString());
    connect(pushButton, &QPushButton::clicked, this, &Authorization::onPressButtonLogin);
}


void Authorization::onPressButtonLogin() {
    if (password->text().isEmpty() || login->text().isEmpty())
        return;
    emit onLogin(login->text(), password->text());
}



















