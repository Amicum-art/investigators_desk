#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

class CentralWidget : public QWidget
{
    Q_OBJECT
protected:
    CentralWidget(QWidget* parent = nullptr);
public:
    virtual ~CentralWidget() = default;
};

inline CentralWidget::CentralWidget(QWidget *parent):
    QWidget(parent)
{
}




#endif // CENTRALWIDGET_H
