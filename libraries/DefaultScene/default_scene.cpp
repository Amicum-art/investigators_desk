#include "default_scene.h"

DefaultScene::DefaultScene(QWidget* parent):
    CentralWidget(parent),
    mainLay(new QVBoxLayout(this)),
    convertViewer(new ConverterViewer<TextFromClipboard, TextToClipboard, DictionaryConverter>(this)),
    calendarMenu(new SubmenuCalendar(this))
{
    mainLay->addWidget(convertViewer);
    mainLay->addWidget(calendarMenu);
    setLayout(mainLay);
}
