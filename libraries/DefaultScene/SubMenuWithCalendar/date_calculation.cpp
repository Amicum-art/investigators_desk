#include "date_calculation.h"


DateCalculation::DateCalculation(QWidget* parent):
    WidgetForSubMenu(parent),
    mLay(new QVBoxLayout(this)),
    lableDate(new QLabel(this)),
    dateEdit(new QDateEdit(this)),
    labelTenDays(new QLabel(this)),
    labelThirdDays(new QLabel(this))
{
    QSizePolicy sizePolicy (QSizePolicy::Expanding, QSizePolicy::Expanding);
    lableDate->setText("Рассчёт дат на\nпродление");
    lableDate->setFont((QString)"qss/TTNorms-Regular.ttf");
    lableDate->setEnabled(false);
    lableDate->setSizePolicy(sizePolicy);
    QString style = "border-radius: 5px;"
             "font: 11pt;"
             "background-color: rgb(204, 203, 204);"
             "color: rgb(55, 55, 55);"
             "QLabel { font-weight: bold; }";
    lableDate->setStyleSheet(style);
    lableDate->setScaledContents(true);
    lableDate->setAlignment(Qt::AlignCenter);
    mLay->addWidget(lableDate);
    dateEdit->setEnabled(true);
    //dateEdit->setMinimumSize(QSize(0, height() * .83));
    dateEdit->setCursor(QCursor(Qt::IBeamCursor));
    dateEdit->setMouseTracking(false);
    dateEdit->setTabletTracking(false);
    dateEdit->setFocusPolicy(Qt::ClickFocus);
    dateEdit->setContextMenuPolicy(Qt::NoContextMenu);
    dateEdit->setAcceptDrops(false);
    dateEdit->setLayoutDirection(Qt::LeftToRight);
    dateEdit->setInputMethodHints(Qt::ImhPreferNumbers);
    dateEdit->setWrapping(false);
    dateEdit->setFrame(false);
    dateEdit->setAlignment(Qt::AlignCenter);
    dateEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
    dateEdit->setAccelerated(false);
    dateEdit->setKeyboardTracking(true);
    dateEdit->setProperty("showGroupSeparator", QVariant(false));
    dateEdit->setCalendarPopup(false);
    dateEdit->setTimeSpec(Qt::LocalTime);
    connect(dateEdit, &QDateEdit::dateChanged, this, &DateCalculation::onAction);
    dateEdit->setDateTime(QDateTime::currentDateTime());
    mLay->addWidget(dateEdit);
    labelTenDays->setEnabled(false);
    labelTenDays->setSizePolicy(sizePolicy);
    labelTenDays->setStyleSheet(style);
    labelTenDays->setAlignment(Qt::AlignCenter);
    mLay->addWidget(labelTenDays);
    labelThirdDays->setEnabled(false);
    labelThirdDays->setSizePolicy(sizePolicy);
    labelThirdDays->setStyleSheet(style);
    labelThirdDays->setAlignment(Qt::AlignCenter);
    mLay->addWidget(labelThirdDays);
    setLayout(mLay);
}

void DateCalculation::changeDate(QDate date)
{
    dateEdit->setDate(date);
}

void DateCalculation::onAction()
{
    auto date = dateEdit->date().addDays(10);
    auto firstDate = date.toString(Qt::ISODateWithMs);
    firstDate = firstDate.last(2) + '.' + firstDate.mid(5, 2) + '.' + firstDate.first(4);
    labelTenDays->setText(firstDate);
    date = date.addDays(20);
    auto secondDate = date.toString(Qt::ISODateWithMs);
    secondDate = secondDate.last(2) + '.' + secondDate.mid(5, 2) + '.' + secondDate.first(4);
    labelThirdDays->setText(secondDate);
}
