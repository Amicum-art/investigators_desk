#include "planer.h"
#include "crypt.h"
#include "edit_widget_for_rec.h"
#include "factory_add_anything.h"


Planer::Planer(const QString& date, QListView *listView):
    FactoryEditMenu<QListView*>(listView),
    currentDate(QStringRef(&date,8, 2) + '.' +
                QStringRef(&date,5, 2) + '.' +
                QStringRef(&date,0, 4))
{
    resize(500, 400);
    auto font = listView->font();
    font.setPointSize(10);
    listView->setFont(font);
    listView->setStyleSheet("background-color: rgba(220,220,220,180);");
    listView->setModel(new QStringListModel(getListForView(), listView));
    listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    setAttribute(Qt::WA_DeleteOnClose);
    connect(getOkBtn(), &QPushButton::clicked, this, [this, listView]() {
        auto edit = new EditWidgetForRec;
        connect(edit, &EditWidgetForRec::accepted, this, [this, listView]()
        {
            listView->setModel(new QStringListModel(getListForView(), listView));
        });
        edit->show();
    });
    connect(getRemoveBtn(), &QPushButton::clicked, this, [this, listView]() {
        const auto index = listView->selectionModel()->currentIndex();
        if(index.isValid()){
            auto record = listView->model()->data(index, Qt::DisplayRole).toString();
            deleteRec(currentDate + ' ' + QStringRef(&record, 0, ((record[2] == ':')? 5: 4)));
            listView->model()->removeRow(index.row());
        }
    });
}

Planer::Records Planer::getRecordForDate()
{
    Records vec;
    for (const QString &directory: {"Текущие уголовные дела", "Текущие материалы проверки"}) {
        if (QFileInfo(directory).isDir()) {
            QDir dir(directory);
            dir.setNameFilters(QStringList("*.txt"));
            dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
            QStringList fileList = dir.entryList();
            for (const auto &file: fileList) {
                auto filePers(directory + '/' + file);
                auto vecWithRec = RecordFileEditor::getCaseRecords(filePers);
                for (const auto &[absoluteDate, event]: vecWithRec) {
                    if (QStringRef(&absoluteDate, 0, 10) == currentDate){
                        auto caseFile = Code::deCrypt(QStringRef(&file, 0, file.size()-4));
                        vec.emplace_back(absoluteDate + " - " +
                                caseFile + " - ", event);
                    }
                }

            }
        }
    }
    return vec;
}

QStringList Planer::getListForView()
{
    QStringList allPlans{};
    auto vec = getRecordForDate();
    allPlans.reserve(vec.size());
    RecordFileEditor::sortRecords(vec);
    for(const auto& v : vec)
    {
        allPlans << (v.dateTime.mid(11) + QStringRef(&v.note));
    }
    return allPlans;
}

void Planer::deleteRec(const QString &date)
{
    Records vecWithRec;
    for (const QString &directory: {"Текущие уголовные дела", "Текущие материалы проверки"})
    {
        if (QFileInfo(directory).isDir())
        {
            QDir dir(directory);
            dir.setNameFilters(QStringList("*.txt"));
            dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
            QStringList fileList = dir.entryList();
            for (const auto &file: fileList)
            {
                auto filePers(directory + "/" + file);
                vecWithRec = RecordFileEditor::getCaseRecords(filePers);
                for (const Record& it : vecWithRec)
                {
                    if (date == it.dateTime)
                    {
                        vecWithRec.removeOne(it);
                        RecordFileEditor::saveRecordsToFile(filePers, vecWithRec);
                        return;
                    }
                }
            }
        }
    }
}



