#ifndef PLANER_H
#define PLANER_H
#include "factory_add_anything.h"
#include "record_edit.h"

class Planer :
        public FactoryEditMenu<QListView*>{
    using Records = QVector<Record>;
public:
    explicit Planer(const QString& dateISODateWithMs =
            QDateTime::currentDateTime().toString(Qt::ISODateWithMs),
            QListView* listView = new QListView);
private:
    Records getRecordForDate();
    QStringList getListForView();
    static void deleteRec(const QString& date);
    QString currentDate;
};
#endif // PLANER_H
