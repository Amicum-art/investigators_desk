#ifndef DATECALCULATION_H
#define DATECALCULATION_H
#include "widget_for_sub_menu.h"

class DateCalculation: public WidgetForSubMenu
{
public:
    DateCalculation(QWidget* parent = nullptr);
    virtual ~DateCalculation() = default;
private:
    virtual void changeDate(QDate date) final;
private slots:
    void onAction();
private:
    QVBoxLayout* mLay;
    QLabel *lableDate;
    QDateEdit *dateEdit;
    QLabel *labelTenDays;
    QLabel *labelThirdDays;
};

#endif // DATECALCULATION_H
