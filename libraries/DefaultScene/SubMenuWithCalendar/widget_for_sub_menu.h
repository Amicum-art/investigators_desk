#ifndef WIDGETFORSUBMENU_H
#define WIDGETFORSUBMENU_H


class WidgetForSubMenu : public QWidget
{
    Q_OBJECT
protected:
    WidgetForSubMenu(QWidget* parent = nullptr);
public:
    virtual ~WidgetForSubMenu() = default;
signals:
    void onChangeDate(QDate);
private slots:
    virtual void changeDate(QDate date) = 0;
};

#endif // WIDGETFORSUBMENU_H
