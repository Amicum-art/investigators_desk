#include "planer_menu.h"
#include "edit_widget_for_rec.h"
#include "planer.h"

PlanerMenu::PlanerMenu(QWidget* parent):
    WidgetForSubMenu(parent),
    lay(new QVBoxLayout(this)),
    addPlan(new QPushButton(this)),
    buttPlan(new QPushButton(this))
{
    QString style = "border-radius: 5px;"
             "font: 11pt;"
             "background-color: rgb(204, 203, 204);"
             "color: rgb(55, 55, 55);"
             "QLabel { background-color: rgba(240,240,240,200); font-weight: bold; }";
    buttPlan->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    buttPlan->setText("План на день");
    buttPlan->setIconSize(QSize(30, 30));
    buttPlan->setToolTip("Выберите дату\nв календаре");
    buttPlan->setIcon(QIcon(QPixmap("qss/default/notes.png")));
    buttPlan->setFont((QString)"qss/TTNorms-Regular.ttf");
    buttPlan->setStyleSheet(style);
    buttPlan->setCursor(QCursor(Qt::PointingHandCursor));
    lay->addWidget(buttPlan);
    addPlan->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    addPlan->setToolTip("Создать заметку");
    addPlan->setIcon(QIcon(QPixmap("qss/default/addP.png")));
    addPlan->setIconSize({35, 45});
    addPlan->setStyleSheet(style);
    addPlan->setCursor(QCursor(Qt::PointingHandCursor));
    connect(addPlan, &QPushButton::clicked, [this]() {
        (new EditWidgetForRec)->show();
    });
    connect(buttPlan, &QPushButton::clicked, this, &PlanerMenu::getPlaner);
    lay->addWidget(addPlan);
}

void PlanerMenu::changeDate(QDate _date)
{
    date = _date.toString(Qt::ISODateWithMs);
}

void PlanerMenu::getPlaner()
{
    (new Planer(date))->show();
}
