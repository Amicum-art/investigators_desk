#ifndef SUBMENUCALENDAR_H
#define SUBMENUCALENDAR_H

#include "factory_add_anything.h"
#include "date_calculation.h"
#include "planer_menu.h"


class SubmenuCalendar : public QWidget
{
    Q_OBJECT
    using MyExchangeClass = ExchangerMenu<PlanerMenu*, DateCalculation*>;
public:
    explicit SubmenuCalendar(QWidget *parent = nullptr);

public slots:
    void exchangeSubMenu();
    void sendDate();
private:
    QHBoxLayout *mainLay;
    QPushButton *exchangeButton;
    std::unique_ptr<MyExchangeClass> mainWidget;
    QCalendarWidget *calendarWidget;
    QStringList toolTips {"Рассчёт дат","Планировщик"};
};

#endif // SUBMENUCALENDAR_H
