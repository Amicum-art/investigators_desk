#include "submenu_calendar.h"
#include "qtextformat.h"
#include <QFontDatabase>
#include <QCalendarWidget>



SubmenuCalendar::SubmenuCalendar(QWidget *parent)
    : QWidget(parent),
      mainLay (new QHBoxLayout()),
      exchangeButton (new QPushButton(this)),
      mainWidget(std::make_unique<MyExchangeClass>( new PlanerMenu(this), new DateCalculation(this)))
{
    QSizePolicy sizePolicy (QSizePolicy::Expanding, QSizePolicy::Expanding);
    exchangeButton->setIcon(QIcon("qss/default/menuIcon/excMh.png"));
    //excMenu->setIconSize(QSize(width() * .3, height()));
    //excMenu->setFixedSize(QSize(width() * .3, height()));
    exchangeButton->setIconSize(QSize(30, 30));
    exchangeButton->setStyleSheet("font: 12pt;"
                           "color: rgb(42, 42, 42);"
                           "background-color: rgba(0, 0, 0, 0);");
    exchangeButton->setCursor(QCursor(Qt::PointingHandCursor));
    exchangeButton->setToolTip(toolTips[0]);
    connect(exchangeButton, &QPushButton::clicked, this, &SubmenuCalendar::exchangeSubMenu);
    mainLay->addWidget(exchangeButton);
    mainLay->addWidget(mainWidget.get());
    calendarWidget = new QCalendarWidget(this);
    calendarWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    //calendarWidget->setFixedSize(QSize(parent->width() * .44, parent->height() * .376));
    calendarWidget->setContextMenuPolicy(Qt::DefaultContextMenu);
    calendarWidget->setLayoutDirection(Qt::LeftToRight);
    calendarWidget->setNavigationBarVisible(true);
    auto currentDay = QTextCharFormat();
    currentDay.setForeground(QBrush(QColor("rgb(255, 250, 216)")));
    QString style = "QToolButton#qt_calendar_prevmonth {"
             "background: transparent;"
             "border: none;"
             "width: 40px;"
             "qproperty-icon: url(qss/default/arrow_left.png);"
             "}"
             "QToolButton#qt_calendar_nextmonth {"
             "background: transparent;"
             "border: none;"
             "width: 40px;"
             "qproperty-icon: url(qss/default/arrow_right.png);"
             "}"
             "QCalendarWidget QHeaderView {"
             "qproperty-minimumSectionSize:0;"
             "}"
             "QCalendarWidget QMenu{"
             "background-color: rgb(133,133,133);"
             "}"
             "#qt_calendar_calendarview {"
             "background-color: rgb(160,162,163);"
             "color: rgb(76,76,67);"
             "border-radius: 7px;"
             "border: 2px solid rgba(76, 74,72,0.6);"
             "selection-background-color: rgb(240, 240, 240);"
             "selection-color: rgb(0, 0, 10);"
             "font: 14px;"
             "}"
             "#qt_calendar_monthbutton {"
             "background-color: rgb(160,162,163);"
             "}"
             "#qt_calendar_yearbutton {"
             "background-color: rgb(160,162,163);"
             "}"
             "#qt_calendar_yearedit {"
             "background-color: rgb(160,162,163);"
             "}"
             "#qt_calendar_navigationbar {"
             "background-color: rgb(160,162,163);"
             "}"
             "QCalendarWidget QAbstractItemView {"
             "color: rgb(160,162,163);"
             "}";
    calendarWidget->setStyleSheet(style);
    calendarWidget->setWeekdayTextFormat(Qt::Saturday, currentDay);
    calendarWidget->setWeekdayTextFormat(Qt::Sunday, currentDay);
    calendarWidget->setGridVisible(false);
    calendarWidget->setSelectionMode(QCalendarWidget::SingleSelection);
    calendarWidget->setHorizontalHeaderFormat(QCalendarWidget::ShortDayNames);
    calendarWidget->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
    calendarWidget->setDateEditEnabled(true);
    mainLay->addWidget(calendarWidget);
    connect(calendarWidget, &QCalendarWidget::selectionChanged, this, &SubmenuCalendar::sendDate);
    setLayout(mainLay);
    sendDate();
}

void SubmenuCalendar::exchangeSubMenu()
{
    static size_t i = 0;
    mainWidget->nextItem();
    exchangeButton->setToolTip(toolTips[++i % toolTips.size()]);
    //QObject::connect(calendarWidget, SIGNAL(selectionChanged()), MainWindow, SLOT(onDate()));
}

void SubmenuCalendar::sendDate()
{
   emit mainWidget->getItem()->onChangeDate(calendarWidget->selectedDate());
}
