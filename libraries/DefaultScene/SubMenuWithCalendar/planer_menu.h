#ifndef PLANERMENU_H
#define PLANERMENU_H
#include "widget_for_sub_menu.h"



class PlanerMenu : public WidgetForSubMenu
{
public:
    PlanerMenu(QWidget* parent = nullptr);
    virtual ~PlanerMenu() = default;
private:
    virtual void changeDate(QDate date) final;
private slots:
    void getPlaner();
private:
    QString date;
    QVBoxLayout* lay;
    QPushButton *addPlan;
    QPushButton *buttPlan;
};

#endif // PLANERMENU_H
