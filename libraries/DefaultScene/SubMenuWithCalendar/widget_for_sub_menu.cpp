#include "widget_for_sub_menu.h"


WidgetForSubMenu::WidgetForSubMenu(QWidget *parent) :
    QWidget(parent)
{
    connect(this, &WidgetForSubMenu::onChangeDate, this, &WidgetForSubMenu::changeDate);
}
