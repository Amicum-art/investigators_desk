#include "text_to_clipboard.h"


TextToClipboard::TextToClipboard(QWidget* parent):
    SaveText(parent)
{
}

void TextToClipboard::saveText(const QString &text)
{
    QApplication::clipboard()->setText(text, QClipboard::Clipboard);
}
