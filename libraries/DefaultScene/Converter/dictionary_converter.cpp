#include "dictionary_converter.h"
#include <QStringRef>
#include <QTextStream>

QString DictionaryConverter::convert(bool isMan, QString text)
{
    phraseConversion(isMan, text);
    return subsequentConversion(isMan, text);
}

void DictionaryConverter::phraseConversion(bool isMan, QString& text)
{
    auto differExchange = (isMan)
            ? std::function<const QString(const pair& p)>([](const pair& p) { return p.first; })
            : std::function<const QString(const pair& p)>([](const pair& p) { return p.second; });
    auto replace = [&](const QString& word, const QString& re_word) {
        int start_pos = 0;
        for (int re = text.indexOf(word, start_pos);
             re != -1 && start_pos < text.size();
             re = text.indexOf(word, start_pos))
        {
            int afterWord = re + word.size();
            if(afterWord > text.size())
                afterWord = text.size();
            if (((!re || text[re - 1] == ' '
                    || text[re - 1] == '.'
                    || text[re - 1] == ','
                    || text[re - 1] == '!'
                    || text[re - 1] == '-'
                    || text[re - 1] == '?'
                    || text[re - 1] == ':'
                    || text[re - 1] == ';'))
                    &&
                    (afterWord == text.size()
                    || text[afterWord] == ','
                    || text[afterWord] == ' '
                    || text[afterWord] == '.'
                    || text[afterWord] == '!'
                    || text[afterWord] == '-'
                    || text[afterWord] == '?'
                    || text[afterWord] == ':'
                    || text[afterWord] == ';'
                    ))
                text = QStringRef(&text, 0, re) + re_word + QStringRef(&text, afterWord, text.size()-afterWord);
            start_pos = re + 1;
        }
    };
    for (const auto& [first, second]: Dictionary::compoundWords) {
        auto originalWord = first;
        auto exchangeWord = differExchange(second);
        replace(originalWord, exchangeWord);
        originalWord[0] = originalWord[0].toUpper();
        exchangeWord[0] = exchangeWord[0].toUpper();
        replace(originalWord, exchangeWord);
    }
}

QString DictionaryConverter::subsequentConversion(bool isMan, QString & text)
{
    QString result, word;
    QChar afterWord;
    auto freq = (isMan)
            ? std::function<QString(QString)>([](QString word) { return Dictionary::frequency.at(word).first; })
            : std::function<QString(QString)>([](QString word) { return Dictionary::frequency.at(word).second; });
    QTextStream stream(&text);
    while (stream >> word, !word.isEmpty()) {
        bool isEnd = true;
        int length = word.length() - 1;
        if ((word[length] == '.' || word[length] == ','
             || word[length] == ':' || word[length] == ';'
             || word[length] == '!' || word[length] == '?')) {
            afterWord = word[length];
            word.chop(1);
            isEnd = false;
        }
        auto lower = word.toLower();
        if (!word.isEmpty() && word != "\n") {
            if (Dictionary::frequency.find(lower) != Dictionary::frequency.end()) {
                auto temp = freq(lower);
                if (word != lower)
                    temp[0] = temp[0].toUpper();
                word = temp;

            }
            if (Dictionary::shafreq.find(lower) != Dictionary::shafreq.end()) {
                auto temp = Dictionary::shafreq.at(lower);
                if (word != lower)
                    temp[0] = temp[0].toUpper();
                word = temp;
            }
        }
        if (!isEnd)
            word += afterWord;
        result += word + ' ';
    }
    result.chop(1);
    return result;
}
