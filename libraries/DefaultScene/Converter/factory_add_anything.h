#ifndef TEMPLATEFORADDSOMETHING_H
#define TEMPLATEFORADDSOMETHING_H

#include "widget_for_sub_menu.h"


namespace styleSh {
const static QString& style {
    "QListView::item {                             \
        background-color: rgb(235,235,235);        \
        color: black;                              \
    }                                              \
    QListView::item:selected {                     \
        background-color: rgba(220,220,215,0.9);   \
        color: black;                              \
    }                                              \
    QComboBox {                                    \
        background-color: rgba(255, 242, 255,4);   \
        border: 1px solid rgb(108,108,108);        \
        color: rgb(78,78,78);                      \
        font: 12pt \"Times New Roman\";            \
    }                                              \
    QDateTimeEdit {                                \
        background-color: rgba(255, 242, 255,4);   \
        border: 1px solid rgb(108,108,108);        \
        color: rgb(78,78,78);                      \
        font: 12pt \"Times New Roman\";            \
    }                                              \
    QLineEdit {                                    \
        background-color: rgba(255, 242, 255,4);   \
        border: 1px solid rgb(108,108,108);        \
        color: rgb(78,78,78);                      \
        font: 12pt \"Times New Roman\";            \
    }"};
}


template <typename... Args>
class AddingCustomWidgets : public QDialog {
public:
    inline AddingCustomWidgets(Args ...args):
        mainLay(new QVBoxLayout(this)),
        layForWidgets(new QHBoxLayout),
        layForButtons (new QHBoxLayout)
    {
        setMinimumWidth(500);
        ([&]{
            layForWidgets->addWidget(args);
        }(), ...);
        mainLay->addLayout(layForWidgets);
        okBtn = new QPushButton("Добавить", this);
        okBtn->setCursor(QCursor(Qt::PointingHandCursor));
        connect(okBtn, &QPushButton::clicked, this, &QDialog::accept);
        layForButtons->addWidget(okBtn);
        cancelBtn = new QPushButton("Назад", this);
        cancelBtn->setCursor(QCursor(Qt::PointingHandCursor));
        connect(cancelBtn, &QPushButton::clicked, this, &QDialog::close);
        layForButtons->addWidget(cancelBtn);
        mainLay->addLayout(layForButtons);
        setLayout(mainLay);
        setWindowIcon(QIcon("qss/main/icon.png"));
        setStyleSheet(styleSh::style);
    };
    inline virtual ~AddingCustomWidgets(){
        while (auto item = layForWidgets->takeAt(0))
        {
            delete item->widget();
            delete item;
        }
    }
    QPushButton* getOkBtn(){ return okBtn; }
    QPushButton* getCancelBtn(){ return cancelBtn; }
private:
    QPushButton* okBtn;
    QPushButton* cancelBtn;
    QVBoxLayout* mainLay;
    QHBoxLayout* layForWidgets;
    QHBoxLayout* layForButtons;
};


template <typename... Args>
class FactoryEditMenu : public QWidget {
public:
    inline FactoryEditMenu(Args ...args):
        mainLay(new QVBoxLayout(this)),
        layForWidgets(new QHBoxLayout),
        layForButtons (new QHBoxLayout)
    {
        setMinimumWidth(200);
        ([&]{
            layForWidgets->addWidget(args);
        }(), ...);
        mainLay->addLayout(layForWidgets);
        okBtn = new QPushButton("Добавить", this);
        okBtn->setCursor(QCursor(Qt::PointingHandCursor));
        layForButtons->addWidget(okBtn);
        removeBtn = new QPushButton("Удалить", this);
        removeBtn->setCursor(QCursor(Qt::PointingHandCursor));
        layForButtons->addWidget(removeBtn);
        mainLay->addLayout(layForButtons);
        setLayout(mainLay);
        setWindowIcon(QIcon("qss/main/icon.png"));
        setStyleSheet(styleSh::style);
    };

    inline virtual ~FactoryEditMenu(){
        while (auto item = layForWidgets->takeAt(0))
        {
            delete item->widget();
            delete item;
        }
    }
    QPushButton* getOkBtn(){ return okBtn; }
    QPushButton* getRemoveBtn(){ return removeBtn; }
signals:
    void onAdd();
    void onRemove();
private:
    QPushButton* okBtn;
    QPushButton* removeBtn;
    QVBoxLayout* mainLay;
    QHBoxLayout* layForWidgets;
    QHBoxLayout* layForButtons;
};




template<typename T>
concept DerivedSubWidget = std::is_base_of<WidgetForSubMenu*, T>::value;

template <class ... Args>
class ExchangerMenu : public QWidget {
public:
    inline ExchangerMenu(Args ... args):
        mainLay(new QVBoxLayout(this)),
        count()
    {
        setLayout(mainLay);
        setWindowIcon(QIcon("qss/main/icon.png"));
        setStyleSheet(styleSh::style);
        ([&]{
            mainWidget.emplace_back(args);
            mainLay->addWidget(args);
        }(), ...);
        for(const auto& item: mainWidget) {
            item->hide();
        }
        mainWidget[0]->show();
    };
    WidgetForSubMenu* getItem(){
        return mainWidget[count];
    }
    void nextItem(){
        mainWidget[count]->hide();
        count = (count + 1) % mainWidget.size();
        mainWidget[count]->show();
    }
private:
    QVBoxLayout* mainLay;
    int count;
    std::vector<WidgetForSubMenu*> mainWidget;
};



#endif // TEMPLATEFORADDSOMETHING_H
