#ifndef SAVETEXTINTERFACE_H
#define SAVETEXTINTERFACE_H

class SaveText: public QPushButton {
    Q_OBJECT
protected:
    SaveText(QWidget* parent = 0); 
public:
    virtual ~SaveText() = default;
private:
    virtual void saveText(const QString& text) = 0;
signals:
    void onSaveText(QString);
};

inline SaveText::SaveText(QWidget *parent):
    QPushButton(parent)
{
    setIconSize(QSize(30, 30));
    setIcon(QIcon(QPixmap("qss/default/copy.png")));
    setToolTip("Копировать текст");
    setStyleSheet("QPushButton { "
                  "background-color: rgba(0,0,0,0);}"
                  "QToolTip { "
                  "color: #ffffff; "
                  "background-color: rgba(76, 74,72,0.6); "
                  "border: 1px solid white; }"
                  "QPushButton:pressed {"
                  "background: #dadbde;"
                  " }");
    setCursor(QCursor(Qt::PointingHandCursor));
    connect(this, &SaveText::onSaveText, this, &SaveText::saveText);
}


#endif // SAVETEXTINTERFACE_H
