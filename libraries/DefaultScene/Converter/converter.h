#ifndef CONVERTER_H
#define CONVERTER_H
#include <QString>

class Converter {
public:
    virtual QString convert(bool isMan, QString text) = 0;
    virtual ~Converter() = default;
};


#endif // CONVERTER_H
