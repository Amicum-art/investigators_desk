#ifndef TEXTTOCLIPBOARD_H
#define TEXTTOCLIPBOARD_H
#include "text_save_interface.h"

class TextToClipboard : public SaveText
{
    Q_OBJECT
public:
    TextToClipboard(QWidget* parent);
    void saveText(const QString& text) final;  
};

#endif // TEXTTOCLIPBOARD_H
