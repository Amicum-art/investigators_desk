#ifndef DICTIONARYCONVERTER_H
#define DICTIONARYCONVERTER_H
#include "converter.h"
#include "dictionary.h"


class DictionaryConverter : public Converter {
    Dictionary dict;
    using pair = const QPair<QString, QString>;
public:
    QString convert(bool isMan, QString text) final;
private:
    static void phraseConversion(bool isMan, QString& text);
    static QString subsequentConversion(bool isMan, QString & text);
    friend class TesterConverter;
};

#endif // DICTIONARYCONVERTER_H
