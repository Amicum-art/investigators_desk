#ifndef DICTIONARY_H
#define DICTIONARY_H
#include <memory>
#include <unordered_map>
#include <QString>
#include <QPair>
#include "insert_to_dict_interface.h"
#include "remove_from_dict_interface.h"

class Dictionary {
    std::unique_ptr<InsertToDictionary> insert;
    std::unique_ptr<RemoverFromDictionary> remove;
public:
    using difVariant = std::unordered_map<QString, QPair<QString, QString>>;
    using generalVariant = std::unordered_map<QString, QString>;
    Dictionary();
    static difVariant compoundWords;
    static difVariant frequency;
    static generalVariant shafreq;
    static void removeFromDictionary(const QString& str);
    static void insertToDictionary(const QString& key, const QString& value);
};



#endif // DICTIONARY_H
