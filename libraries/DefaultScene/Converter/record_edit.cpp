#include "record_edit.h"
#include "crypt.h"

std::variant<bool, QString> RecordFileEditor::addRecord (const Record& record,
                                                        const QString& filePath)
{
    if(record.dateTime.isEmpty()
            || record.note.isEmpty()
            || !QFile(filePath).exists())
        return false;
    if(auto uniq = checkUniqDate(record.dateTime); uniq.has_value())
    {
        return uniq.value();
    } else {
        Records vecWithRec = getCaseRecords(filePath);
        vecWithRec.emplace_back(record);
        sortRecords(vecWithRec);
        saveRecordsToFile(filePath, vecWithRec);
        return true;
    }
}

bool RecordFileEditor::renameRecord(const Record& record, const QString& filePath)
{
    if(record.dateTime.isEmpty()
            || record.note.isEmpty()
            || !QFile(filePath).exists())
        return false;
    Records vecWithRec = getCaseRecords(filePath);
    for (auto &[date, note]: vecWithRec)
    {
        if (record.dateTime == date)
        {
            note = record.note;
            date = record.dateTime;
            break;
        }
    }
    sortRecords(vecWithRec);
    saveRecordsToFile(filePath, vecWithRec);
    return true;
}

bool RecordFileEditor::copyRecords(const QString& filePath)
{
    if(QFile(filePath).exists())
    {
        auto vecWithRec = getCaseRecords(filePath);
        QString result;
        for (const auto &[date, note]: vecWithRec)
            result += date + " - " + note + "\n";
        QApplication::clipboard()->setText(result, QClipboard::Clipboard);
        return true;
    }
    return false;
}

bool RecordFileEditor::deleteRecord(const QString& dateTime,
                                    const QString& filePath)
{
    if(QFile(filePath).exists())
    {
        auto vec = getCaseRecords(filePath);
        for (const auto& record : vec)
        {
            if (dateTime == record.dateTime)
            {
                vec.removeOne(record);
                break;
            }
        }
        saveRecordsToFile(filePath, vec);
        return true;
    }
    return false;
}

std::optional<QString> RecordFileEditor::checkUniqDate(const QString &date)
{
    Records vecWithRec;
    for (const QString &directory: {"Текущие уголовные дела", "Архивные уголовные дела",
            "Текущие материалы проверки", "Архивные материалы проверки"})
    {
        if (QFileInfo(directory).isDir())
        {
            QDir dir(directory);
            dir.setNameFilters(QStringList("*.txt"));
            dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
            QStringList fileList = dir.entryList();
            for (const auto &file: fileList)
            {
                auto filePers(directory + "/" + file);
                vecWithRec = getCaseRecords(filePers);
                for (const auto& [_date, note] : vecWithRec)
                {
                    if (date == _date)
                    {
                        auto person = QStringRef(&file, 0, file.size()-4);
                        return _date + " - " + Code::deCrypt(person)
                                + " - " + note;
                    }
                }
            }
        }
    }
    return {};
}

RecordFileEditor::Records RecordFileEditor::getCaseRecords(const QString &filePath)
{
    Records vec{};
    QFile file(filePath);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        QStringList dateAndNote;
        while(!stream.atEnd())
        {
            dateAndNote = Code::deCrypt(stream.readLine()).split("&");
            vec.emplace_back(dateAndNote.first(),dateAndNote.last());
        }
        file.close();
    }
    return vec;
}

void RecordFileEditor::sortRecords(Records &vecRec)
{
    std::ranges::sort(vecRec, std::less<>{}, [](const Record& f)
    {
        return QDateTime::fromString(f.dateTime,(f.dateTime.size() == 16)
                                     ?"dd.MM.yyyy HH:mm" :"dd.MM.yyyy H:mm");
    });
}

void RecordFileEditor::saveRecordsToFile(const QString &fileName,
                                         const Records &records)
{
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly |
                  QIODevice::Text |
                  QIODevice::Truncate))
    {
        QTextStream stream(&file);
        QString str{};
        for (const auto &[date,note]: records)
            str += Code::crypt(QStringRef(&date) + '&' + QStringRef(&note)) + '\n';
        stream << str;
        file.close();
    }
}


Record::Record(const QString &_dateTime, const QString &_note)
    : dateTime(_dateTime), note(_note) {}

bool Record::operator== (const Record &oth) const
{
    return dateTime == oth.dateTime;
}
