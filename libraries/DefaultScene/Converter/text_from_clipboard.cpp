#include "text_from_clipboard.h"


TextFromClipboard::TextFromClipboard(QWidget* prt) :
    ReceiveText(prt)
{}

void TextFromClipboard::getText()
{
    emit onGetText(QApplication::clipboard()->text());
}
