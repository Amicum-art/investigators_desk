#include "dictionary.h"
#include <fstream>
#include <QFile>
#include <QTextStream>

Dictionary::Dictionary()
{
}

void Dictionary::removeFromDictionary(const QString &str)
{
    shafreq.erase(str);
}

void Dictionary::insertToDictionary(const QString &key, const QString &value)
{
    shafreq.insert({key, value});
}

Dictionary::difVariant Dictionary::compoundWords = []()
{
    std::unordered_map<QString, QPair<QString, QString>> myMap;
    QFile file("compoundWords.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList parts = line.split(',');
            if (parts.count() == 3)
            {
                myMap[parts[0]] = { parts[1], parts[2] };
            }
        }
        file.close();
    }
    return myMap;
}();

Dictionary::difVariant Dictionary::frequency = []()
{
    std::unordered_map<QString, QPair<QString, QString>> myMap;
    QFile file("frequency.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList parts = line.split(' ');
            if (parts.count() == 3)
            {
                myMap[parts[0]] = { parts[1], parts[2] };
            }
        }
        file.close();
    }
    return myMap;
}();

Dictionary::generalVariant Dictionary::shafreq = []()
{
    std::unordered_map<QString, QString> myMap;
    QFile file("shafreq.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList parts = line.split(' ');
            if (parts.count() == 2)
            {
                myMap[parts[0]] = parts[1];
            }
        }
        file.close();
    }
    return myMap;
}();
