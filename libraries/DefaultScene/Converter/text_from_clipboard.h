#ifndef TEXTFROMCLIPBOARD_H
#define TEXTFROMCLIPBOARD_H
#include "text_receiving_interface.h"


class TextFromClipboard : public ReceiveText
{
    Q_OBJECT
public:
    TextFromClipboard(QWidget* prt = 0);
private:
    void getText() final;
};


#endif // TEXTFROMCLIPBOARD_H
