#ifndef DICTIONARYTEMPLATES_H
#define DICTIONARYTEMPLATES_H

#include "factory_add_anything.h"


class DictionaryTemplates :
        public FactoryEditMenu<QListView*>
{
public:
    using WordExchanging = AddingCustomWidgets<QLineEdit*, QLineEdit*>;
    DictionaryTemplates(QListView* listView = new QListView);
    static void updateVoc();
private:
    static QStringList getData();
    void removeTemplate(QListView* list);
    void addNewTemplate(QListView* list, QLineEdit* from, QLineEdit* to);
    std::unique_ptr<WordExchanging> forNewWord;
};



#endif // DICTIONARYTEMPLATES_H
