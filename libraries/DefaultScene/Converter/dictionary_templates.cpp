#include "dictionary_templates.h"
#include "crypt.h"
#include "dictionary.h"


DictionaryTemplates::DictionaryTemplates(QListView* listView):
        FactoryEditMenu<QListView*>(listView)
{
    auto screen = QGuiApplication::screens().constFirst()->availableGeometry();
    resize(screen.width() * .256, screen.height() * .208);
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowIcon(QIcon("qss/main/icon.png"));
    listView->setModel(new QStringListModel(getData(), this));
    listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    auto font = listView->font();
    font.setPointSize(10);
    listView->setFont(font);
    getOkBtn()->setText("Добавить слово");
    connect(getOkBtn(), &QPushButton::clicked, this, [this, listView]() {
        auto from = new QLineEdit();
        auto to = new QLineEdit();
        forNewWord = std::make_unique<WordExchanging>(from, to);
        forNewWord->setWindowTitle("Введите пару для замены");
        connect(forNewWord.get(), &WordExchanging::accepted,
                this, [this, listView, from, to]()
        {
            addNewTemplate(listView, from, to);
        });
        forNewWord->show();
    });
    connect(getRemoveBtn(), &QPushButton::clicked,
            this, [this, listView]() { removeTemplate(listView);
    });
    updateVoc();
}

void DictionaryTemplates::updateVoc() {
    auto list = getData();
    static std::string str = " => ";
    QStringList twoWords;
    for (const auto &string: list) {
        twoWords = string.split(" => ");
        Dictionary::insertToDictionary(twoWords[0].trimmed().toLower(),
                                    twoWords[1].trimmed().toLower());
    }
}


QStringList DictionaryTemplates::getData() {
    QFile file("voca.txt");
    QStringList words;
    if(file.open(QIODevice::ReadOnly)){
        while (!file.atEnd()) {
            QString line = file.readLine();
            line.chop(1);

            line = Code::deCrypt(QStringRef(&line));
            if (!line.isEmpty())
                words << line;
        }
        file.close();
    }
    return words;
}

void DictionaryTemplates::removeTemplate(QListView *list)
{
    auto str = list->model()->data(list->currentIndex()).toString();
    auto idx = str.indexOf(" => ");
    str = str.first(idx);
    Dictionary::removeFromDictionary(str);
    list->model()->removeRow(list->currentIndex().row());
    QFile file("voca.txt");
    if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)) {
        QTextStream stream(&file);
        const auto& end = list->model()->rowCount();
        for (auto it = 0; it < end; it++) {
            QString str = list->model()->index(it, 0).data(Qt::DisplayRole).toString();
            str = Code::crypt(QStringRef(&str)) + '\n';
            stream << str;
        }
        file.close();
    }
}

void DictionaryTemplates::addNewTemplate(QListView *list, QLineEdit *from, QLineEdit *to)
{
    if (!from->text().isEmpty()
        && !to->text().isEmpty()) {
        auto text = from->text().trimmed() + " => " + to->text().trimmed();
        list->model()->insertRow(list->model()->rowCount());
        QModelIndex index = list->model()->index(list->model()->rowCount() - 1, 0);
        list->model()->setData(index, text);
        QFile file("voca.txt");
        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)) {
            QTextStream stream(&file);
            const auto& end = list->model()->rowCount();
            for (auto it = 0; it < end; it++) {
                QString str = list->model()->index(it, 0).data(Qt::DisplayRole).toString();
                str = Code::crypt(QStringRef(&str)) + '\n';
                stream << str;
            }
            file.close();
        }
        updateVoc();
    }
}

