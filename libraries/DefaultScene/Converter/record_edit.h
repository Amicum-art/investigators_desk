#ifndef RECORDEDIT_H
#define RECORDEDIT_H


struct Record {
    QString dateTime, note;
    Record (const QString& _dateTime, const QString& _note);
    Record (const Record& rec) = default;
    bool operator== (const Record& oth) const;
};

class RecordFileEditor
{
    using Records = QVector<Record>;
public:
    RecordFileEditor() = default;
    virtual ~RecordFileEditor() = default;
    static std::variant<bool, QString> addRecord (const Record& record,
                                                const QString& filePath);
    static bool renameRecord (const Record& record,
                            const QString& filePath);
    static bool copyRecords (const QString& filePath);
    static bool deleteRecord (const QString& dateTime,
                            const QString& filePath);
    static std::optional<QString> checkUniqDate (const QString& date);
    static Records getCaseRecords (const QString& filePath);
    static void sortRecords (Records& vecRec);
    static void saveRecordsToFile (const QString& fileName,
                                const Records& records);
};

#endif // RECORDEDIT_H
