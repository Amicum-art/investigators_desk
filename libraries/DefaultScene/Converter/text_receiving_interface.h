#ifndef TEXTRECEIVINGINTERFACE_H
#define TEXTRECEIVINGINTERFACE_H

class ReceiveText : public QPushButton
{
    Q_OBJECT
protected:
    ReceiveText(QWidget* parent = 0);
public:
    virtual ~ReceiveText() = default;
private:
    virtual void getText() = 0;
signals:
    virtual void onGetText(const QString);
};

inline ReceiveText::ReceiveText(QWidget* parent):
    QPushButton(parent)
{
    setIconSize(QSize(30, 30));
    setIcon(QIcon("qss/default/insert.png"));
    setToolTip("Загрузить текст");
    setStyleSheet("QPushButton { "
                  "background-color: rgba(0,0,0,0);}"
                  "QToolTip { "
                  "color: #ffffff; "
                  "background-color: rgba(76, 74,72,0.6); "
                  "border: 1px solid white; }"
                  "QPushButton:pressed {"
                  "background: #dadbde;"
                  " }");
    setCursor(QCursor(Qt::PointingHandCursor));
    connect(this, &QPushButton::clicked, this, &ReceiveText::getText);
}


#endif // TEXTRECEIVINGINTERFACE_H
