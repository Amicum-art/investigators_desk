#ifndef CONVERTERVIEW_H
#define CONVERTERVIEW_H
#include "text_save_interface.h"
#include "text_receiving_interface.h"
#include "converter.h"
#include "dictionary_templates.h"

template <typename receiveTextClass, typename SaveTextClass, typename ConvertTextClass>
class ConverterViewer: public QWidget
{
public:
    ConverterViewer(QWidget* parent = 0);
private slots:
    void setTextToDisplay(const QString text);
    void getTextFromDisplay();
    void loadWidgetForWords();
    void convert();
    void clearDisplay();
private:
    std::unique_ptr<ReceiveText> receiveTextButton;
    std::unique_ptr<SaveText> saveTextButton;
    std::unique_ptr<Converter> converter;
    QPushButton* removeTextButton;
    QPushButton* newWordButton;
    QWidget* wForButtons;
    QTextEdit *display;
    QVBoxLayout* layForButtons;
    QHBoxLayout* layForDisplay;
    QVBoxLayout* mainLay;
    QHBoxLayout* layForConvert;
    QRadioButton* checkMan;
    QPushButton* conv_butt;
    QRadioButton* checkWoman;
};

template<typename receiveTextClass, typename SaveTextClass, typename ConvertTextClass>
ConverterViewer<receiveTextClass, SaveTextClass, ConvertTextClass>::ConverterViewer(QWidget *parent) :
    QWidget(parent),
    receiveTextButton(std::make_unique<receiveTextClass>(this)),
    saveTextButton(std::make_unique<SaveTextClass>(this)),
    converter(std::make_unique<ConvertTextClass>()),
    removeTextButton(new QPushButton(this)),
    newWordButton(new QPushButton(this)),
    wForButtons(new QWidget(this)),
    display(new QTextEdit(this)),
    checkMan(new QRadioButton(this)),
    conv_butt(new QPushButton(this)),
    checkWoman(new QRadioButton(this)),
    layForButtons(new QVBoxLayout()),
    layForDisplay(new QHBoxLayout()),
    layForConvert(new QHBoxLayout()),
    mainLay(new QVBoxLayout(this))
{
    layForButtons->addWidget(receiveTextButton.get());
    layForButtons->addWidget(saveTextButton.get());
    removeTextButton->setToolTip("Очистить поле ввода");
    removeTextButton->setIcon(QIcon(QPixmap("qss/default/delete.png")));
    removeTextButton->setStyleSheet("QPushButton { "
                                    "background-color: rgba(0,0,0,2);}"
                                    "QToolTip { "
                                    "color: #ffffff;"
                                    "background-color: rgba(76, 74,72,0.6); "
                                    "border: 1px solid white; }"
                                    "QPushButton:pressed {"
                                    "    background: #dadbde;"
                                    " }");
    removeTextButton->setCursor(QCursor(Qt::PointingHandCursor));
    removeTextButton->setIconSize(QSize(30, 30));
    layForButtons->addWidget(removeTextButton);
    newWordButton->setToolTip("Собственный список автозамены");
    newWordButton->setIcon(QIcon(QPixmap("qss/default/change.png")));
    newWordButton->setStyleSheet("QPushButton { "
                                 "background-color: rgba(0,0,0,2);}"
                                 "QToolTip { "
                                 "color: #ffffff; "
                                 "background-color: rgba(76, 74,72,0.6); "
                                 "border: 1px solid white; }"
                                 "QPushButton:pressed {"
                                 "background: #dadbde;"
                                " }");
    newWordButton->setCursor(QCursor(Qt::PointingHandCursor));
    newWordButton->setIconSize(QSize(30, 30));
    layForButtons->addWidget(newWordButton);
    wForButtons->setLayout(layForButtons);
    wForButtons->setStyleSheet("border-radius: 10px;"
                     "background-color: rgba(0,0,0,0.2);");
    layForDisplay->addWidget(wForButtons);
    layForDisplay->addWidget(display);
    display->viewport()->setProperty("cursor", QVariant(QCursor(Qt::IBeamCursor)));
    display->setOverwriteMode(true);
    display->setAlignment(Qt::AlignJustify);
    display->setStyleSheet("border-radius: 10px;"
                           "font: 10pt;"
                           "color: rgba(76, 74,72,1);"
                           "border-color: rgb(154, 255, 126);"
                           "background-color: rgba(255, 245,235,0.5);");
    mainLay->addLayout(layForDisplay);

    checkMan->setText("Мужчина");
    checkMan->setCursor(QCursor(Qt::PointingHandCursor));
    checkMan->setStyleSheet("padding-left: 5px;"
                            "border-radius: 7px;"
                            "font: 75 12pt \"Segoe Print\";"
                            "color: #fffad8;"
                            "background-color: rgba(128, 128, 255, 0.6);");
    checkMan->setChecked(true);
    layForConvert->addWidget(checkMan);
    layForConvert->addSpacerItem(new QSpacerItem(10, 1,
            QSizePolicy::Expanding, QSizePolicy::Minimum));
    conv_butt->setText("  Конвертировать");
    conv_butt->setCursor(QCursor(Qt::PointingHandCursor));
    conv_butt->setAutoDefault(true);
    conv_butt->setIcon(QIcon(QPixmap("qss/default/load.png")));
    conv_butt->setStyleSheet("font: 12pt;"
                             "color: rgb(42, 42, 42);"
                             "background-color: rgb(255, 255, 191);");
    layForConvert->addWidget(conv_butt);
    layForConvert->addSpacerItem(new QSpacerItem(10, 1,
            QSizePolicy::Expanding, QSizePolicy::Minimum));
    checkWoman->setText("Женщина");
    checkWoman->setCursor(QCursor(Qt::PointingHandCursor));
    checkWoman->setStyleSheet("padding-left: 5px;"
            "border-radius: 7px;"
            "font: 75 12pt \"Segoe Print\";"
            "color: #fffad8;"
            "background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:0.0965909,"
            "stop:0.468927 rgba(238, 156, 167, 255), stop:1 rgba(255, 221, 225, 255));");
    layForConvert->addWidget(checkWoman);
    mainLay->addLayout(layForConvert);
    setLayout(mainLay);
    connect(conv_butt, &QPushButton::clicked, this, &ConverterViewer::convert);
    connect(receiveTextButton.get(), &ReceiveText::onGetText, this, &ConverterViewer::setTextToDisplay);
    connect(saveTextButton.get(), &QPushButton::clicked, this, &ConverterViewer::getTextFromDisplay);
    connect(removeTextButton, &QPushButton::clicked, this, &ConverterViewer::clearDisplay);
    connect(newWordButton, &QPushButton::clicked, this, &ConverterViewer::loadWidgetForWords);
}


template<typename receiveTextClass, typename SaveTextClass, typename ConvertTextClass>
void ConverterViewer<receiveTextClass, SaveTextClass, ConvertTextClass>::setTextToDisplay(const QString text)
{
    display->setText(text);
}

template<typename receiveTextClass, typename SaveTextClass, typename ConvertTextClass>
void ConverterViewer<receiveTextClass, SaveTextClass, ConvertTextClass>::getTextFromDisplay()
{
    emit saveTextButton->onSaveText(display->toPlainText());
}

template<typename receiveTextClass, typename SaveTextClass, typename ConvertTextClass>
void ConverterViewer<receiveTextClass, SaveTextClass, ConvertTextClass>::loadWidgetForWords()
{
    (new DictionaryTemplates())->show();
};

template<typename receiveTextClass, typename SaveTextClass, typename ConvertTextClass>
void ConverterViewer<receiveTextClass, SaveTextClass, ConvertTextClass>::convert()
{
    auto text = display->toPlainText();
    if(text.isEmpty())
        return;
    text = converter->convert(checkMan->isChecked(), std::move(text));
    display->setText(text);
}


template<typename receiveTextClass, typename SaveTextClass, typename ConvertTextClass>
void ConverterViewer<receiveTextClass, SaveTextClass, ConvertTextClass>::clearDisplay()
{
    display->clear();
}



#endif // CONVERTERVIEW_H
