#ifndef INSERTTODICTIONARY_H
#define INSERTTODICTIONARY_H
#include <QObject>

class InsertToDictionary: public QObject {
    Q_OBJECT
protected:
    InsertToDictionary();
public:
    virtual ~InsertToDictionary() = default;
signals:
    void onInsertPair(std::string, std::string);
private:
    virtual void getPair() = 0;
};

inline InsertToDictionary::InsertToDictionary(){}

#endif // INSERTTODICTIONARY_H
