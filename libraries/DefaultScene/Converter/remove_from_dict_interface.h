#ifndef REMOVEFROMDICTIONARY_H
#define REMOVEFROMDICTIONARY_H
#include <QObject>

class RemoverFromDictionary: public QObject
{
    Q_OBJECT
protected:
    RemoverFromDictionary();
public:
    virtual ~RemoverFromDictionary() = default;
signals:
    void onRemovePair(std::string, std::string);
private:
    virtual void getPair() = 0;
};


inline RemoverFromDictionary::RemoverFromDictionary() {}

#endif // REMOVEFROMDICTIONARY_H
