#ifndef DEFAULTSCENE_H
#define DEFAULTSCENE_H
#include "central_widget.h"
#include "converter_view.h"
#include "submenu_calendar.h"
#include "text_to_clipboard.h"
#include "text_from_clipboard.h"
#include "dictionary_converter.h"

class DefaultScene : public CentralWidget
{
    Q_OBJECT
public:
    DefaultScene(QWidget* parent = 0);
    ~DefaultScene() = default;
private:
    QVBoxLayout* mainLay;
    ConverterViewer<TextFromClipboard, TextToClipboard, DictionaryConverter>* convertViewer;
    SubmenuCalendar* calendarMenu;
};

#endif // DEFAULTSCENE_H
