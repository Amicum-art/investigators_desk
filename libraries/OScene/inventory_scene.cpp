#include "inventory_scene.h"
#include "crypt.h"

Inventory_scene::Inventory_scene(QMainWindow *MainWindow) :
    CentralWidget(MainWindow),
    mainLay(new QHBoxLayout(this)),
    centralLay(new QVBoxLayout()),
    layForTop(new QHBoxLayout()),
    label(new QLabel("Опись",this)),
    hForConsTop(new QHBoxLayout()),
    prevConsist(new QLabel("документов, содержащихся в ", this)),
    dOrM(new QComboBox(this)),
    number_(new QLineEdit(this)),
    tableWidget(new MyTableWidget(this)),
    toolbar(new QToolBar(this))
{

    setStyleSheet(QString::fromUtf8("background-color: rgba(56, 56, 76, 0);"));
    label->setEnabled(false);
    label->setStyleSheet(QString::fromUtf8("color: rgb(76,76,76);"
                                           "font: 700 14pt \"Times New Roman\";"));
    layForTop->addSpacerItem(new QSpacerItem(10, 4, QSizePolicy::Expanding, QSizePolicy::Minimum));
    layForTop->addWidget(label);
    layForTop->addSpacerItem(new QSpacerItem(10, 4, QSizePolicy::Expanding, QSizePolicy::Minimum));
    centralLay->addLayout(layForTop);
    prevConsist->setEnabled(false);
    prevConsist->setStyleSheet(QString::fromUtf8("color: rgb(78,78,78);"
                                             "font: 12pt \"Times New Roman\";"));
    hForConsTop->addSpacerItem(new QSpacerItem(10, 4, QSizePolicy::Expanding, QSizePolicy::Minimum));
    hForConsTop->addWidget(prevConsist);
    dOrM->addItem(QString("уголовном деле №"));
    dOrM->addItem(QString("материвалах проверки №"));
    dOrM->setEditable(true);
    dOrM->lineEdit()->setReadOnly(true);
    dOrM->lineEdit()->setAlignment(Qt::AlignHCenter);
    dOrM->setAutoFillBackground(true);
    dOrM->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 242, 255,4);\n"
                                          "border: 1px solid rgb(108,108,108);"
                                          "color: rgb(78,78,78);\n"
                                          "font: 12pt \"Times New Roman\";"
    ));
    auto *listView = new QListView(dOrM);
    listView->setStyleSheet("QListView::item {                              \
                                 background-color: rgb(235,235,235);            \
                                 color: black; }                                \
                                 QListView::item:selected {                     \
                                 background-color: rgba(220,220,215,0.9);       \
                                 color: black;                                  \
                                }                                               \
                                ");
    dOrM->setView(listView);
    dOrM->setCursor(QCursor(Qt::PointingHandCursor));
    hForConsTop->addWidget(dOrM);
    number_->setStyleSheet(QString::fromUtf8("color:rgb(78,78,78);"
                                             "font: 12pt \"Times New Roman\";"
                                             ""));
    number_->setMaximumWidth(200);
    hForConsTop->addWidget(number_);
    hForConsTop->addSpacerItem(new QSpacerItem(10, 4, QSizePolicy::Expanding, QSizePolicy::Minimum));
    centralLay->addLayout(hForConsTop);
    centralLay->addWidget(tableWidget);
    const auto& styleF = "QPushButton { background-color: rgba(0,0,0,2);}"
                    "QToolTip { "
                        "color: #ffffff; "
                         "background-color: rgba(76, 74,72,0.6); "
                         "border: 1px solid white; "
                    "}"
                    "QPushButton:pressed {""background: #dadbde;}";
    toolbar->setFloatable(false);
    toolbar->setMovable(false);
    toolbar->setOrientation(Qt::Vertical);
    toolbar->setAcceptDrops(false);
    toolbar->setContextMenuPolicy(Qt::PreventContextMenu);
    auto saveO = new QPushButton(this);
    saveO->setToolTip("Сохранить");
    saveO->setStyleSheet(styleF);
    saveO->setIcon(QIcon(QPixmap("qss/Op/save.png")));
    saveO->setIconSize({25, 25});
    saveO->setCursor(QCursor(Qt::PointingHandCursor));
    connect(saveO, &QPushButton::clicked, this, &Inventory_scene::saveIn);
    toolbar->addWidget(saveO);

    auto loadSave = new QPushButton(this);
    loadSave->setToolTip("Сохраненные описи");
    loadSave->setStyleSheet(styleF);
    loadSave->setIcon(QIcon(QPixmap("qss/Op/loadSave.png")));
    loadSave->setIconSize({25, 25});
    loadSave->setCursor(QCursor(Qt::PointingHandCursor));
    connect(loadSave, &QPushButton::clicked, this, [this](){
        auto view = new ViewAllO;
        connect(view, &ViewAllO::onLoad, this, &Inventory_scene::loadO);
    });
    toolbar->addWidget(loadSave);


    auto toWord = new QPushButton(this);
    toWord->setToolTip("Перенести в Word");
    toWord->setStyleSheet(styleF);
    toWord->setIcon(QIcon(QPixmap("qss/Op/word.png")));
    toWord->setIconSize({25, 25});
    toWord->setCursor(QCursor(Qt::PointingHandCursor));
    connect(toWord, &QPushButton::clicked, this, [this]() {
        saveInWord();
    });
    toolbar->addWidget(toWord);
    auto addTempl = new QPushButton(this);
    addTempl->setToolTip("Шаблоны");
    addTempl->setStyleSheet(styleF);
    addTempl->setIcon(QIcon(QPixmap("qss/Op/templ.png")));
    addTempl->setIconSize({25, 25});
    addTempl->setCursor(QCursor(Qt::PointingHandCursor));
    connect(addTempl, &QPushButton::clicked, this, [this](){
        connect(new ViewForTempl, &ViewForTempl::updateModel, tableWidget, &MyTableWidget::updadeCompleter);
    });
    toolbar->addWidget(addTempl);
    auto sha = new QPushButton(this);
    sha->setToolTip("Шапка описи");
    sha->setIcon(QIcon(QPixmap("qss/Op/me.png")));
    sha->setIconSize({30, 30});
    sha->setStyleSheet(styleF);
    sha->setCursor(QCursor(Qt::PointingHandCursor));
    connect(sha, &QPushButton::clicked, this, []() {
        (new ViewForShap)->show();
    });
    toolbar->addWidget(sha);
    auto inf = new QPushButton(this);
    inf->setToolTip("Помощь");
    inf->setIcon(QIcon(QPixmap("qss/Op/help.png")));
    inf->setIconSize({25, 25});
    inf->setStyleSheet(styleF);
    inf->setCursor(QCursor(Qt::PointingHandCursor));
    connect(inf, &QPushButton::clicked, this, []() {
        auto p = new PopUp();
        p->setPopupText("Горячие клавиши набора:\n"
                        "Сtrl+R - добавить строку\n"
                        "Ctrl+D - удалить строку\n"
                        "Сtrl+E - все шаблоны\n"
                        "Ctrl+Tab - следующая ячейка");
        p->show();
    });
    toolbar->addWidget(inf);
    mainLay->addLayout(centralLay);
    mainLay->addWidget(toolbar);

    setLayout(mainLay);
}


void Inventory_scene::saveIn() {
    auto lineEdit = new QLineEdit();
    addTitle = std::make_unique<descriptionTitle>(lineEdit);
    addTitle->setWindowTitle("ВВедите название описи");
    connect(addTitle.get(), &descriptionTitle::accepted, this, [this, lineEdit](){
        QString currNameFile = lineEdit->text();
        if (currNameFile.isEmpty())
            return;
        if (!QDir("systemO").exists())
            QDir().mkdir(QString::fromStdString("systemO"));
        auto path{(QString) "systemO" + '/' + Code::crypt(QStringRef(&currNameFile)) + ".txt"};
        QFile file(path);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)) {
            QTextStream stream(&file);
            stream << Code::crypt(dOrM->currentText() + '&' + number_->text()) + '\n';
            auto vecTableRec = tableWidget->getText();
            for(const auto& item: vecTableRec)
                stream << Code::crypt(item.first + '&' + item.second) + '\n';
            file.close();
        }
        addTitle.reset();
    });
    addTitle->show();
}

void Inventory_scene::loadO(QString name)
{
    QFile file("systemO/" + name + QString::fromStdString(".txt"));
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream stream(&file);
        auto string = Code::deCrypt(stream.readLine()).split("&");
        dOrM->setCurrentText(string.first());
        number_->setText(string.last());
        QVector<QStringList> vec;
        while (!stream.atEnd()) {
            string = Code::deCrypt(stream.readLine()).split("&");
            vec.emplace_back(string);
        }
        tableWidget->setNewText(vec);
    }
}


QString getWordInstallPath()
{
    QString wordPath = QStandardPaths::findExecutable("winword.exe");
    if (!wordPath.isEmpty()) {
        return wordPath;
    }
    return {};
}

void Inventory_scene::saveInWord() const {
    HKEY hKey;
    LONG regOpenResult = RegOpenKeyExW(HKEY_CLASSES_ROOT, L"Word.Application\\CurVer", 0, KEY_READ, &hKey);
    if (regOpenResult == ERROR_SUCCESS) {
        RegCloseKey(hKey);
        HWND hWnd = FindWindow(NULL, TEXT("Microsoft Word"));
        QProcess process;
        if (!hWnd) {
            process.start(getWordInstallPath());
            process.waitForStarted();
        };
        if (auto *word = new QAxObject("Word.Application");!word->isNull()) {
            word->dynamicCall("SetDisplayAlerts(bool)", false);
            QAxObject *docs = word->querySubObject("Documents");
            auto doc = docs->querySubObject("Add()");
            doc->querySubObject("Range()")->querySubObject("ParagraphFormat")->setProperty("SpaceAfter", 1);
            QAxObject *selection = word->querySubObject("Selection()"); //выделяем область
            QAxObject *font = selection->querySubObject("Font()");
            font->setProperty("Size", 12); //размер заголовка
            font->setProperty("Bold", 2); //жирный
            font->setProperty("Name", "Times New Roman");
            QAxObject *pswds = selection->querySubObject("ParagraphFormat ()");
            pswds->dynamicCall("SetAlignment (WdParagraphAlignment)", 1); // по центру
            selection->setProperty("Alignment", 1);
            selection->dynamicCall("TypeText(const QString&)", "ОПИСЬ"); //записываем текст
            selection->dynamicCall("TypeParagraph()");
            process.close();
            QAxObject *selection2 = word->querySubObject("Selection()"); //область для другого текста
            QAxObject *font2 = selection2->querySubObject("Font()");
            font2->setProperty("Size", 12);
            font2->setProperty("Bold", 0);  // не жирный
            font2->setProperty("Name", "Times New Roman");
            QAxObject *pswds2 = selection2->querySubObject("ParagraphFormat ()");
            pswds2->dynamicCall("SetAlignment (WdParagraphAlignment)", 1); // по центру
            selection2->setProperty("Alignment", 1);
            selection2->dynamicCall("TypeText(const QString&)", (prevConsist->text()
                                                                 + dOrM->currentText()
                                                                 + number_->text()));
            int prob = label->text().size() + prevConsist->text().size()
                    + dOrM->currentText().size() + number_->text().length();
            auto range4 = doc->querySubObject("Range()");
            range4->dynamicCall("SetRange(int,int)", prob + 3, prob + 100);
            auto al = range4->querySubObject("ParagraphFormat ()");
            al->dynamicCall("SetAlignment (WdParagraphAlignment)", 3); // по центру
            {
                const auto& shap = ViewForShap::getShapFromFile();
                if(!shap.isEmpty()){
                    QString result = "\n" + shap[0] ;
                    int spacers = 124 - (shap[0].size()-shap[0].lastIndexOf('\n')) - shap[1].length();
                    result += QString(spacers,' ') + shap[1];
                    range4->setProperty("Text", result);
                    range4->dynamicCall("InsertParagraphAfter()");
                }
            }
            QAxObject *selection3 = word->querySubObject("Selection");
            QAxObject *font3 = selection3->querySubObject("Font()");
            font3->setProperty("Size", 12);
            font3->setProperty("Bold", 0);
            font3->setProperty("Name", "Times New Roman");
            QAxObject *pswds3 = selection3->querySubObject("ParagraphFormat ()");
            pswds3->dynamicCall("SetAlignment (WdParagraphAlignment)", 1); //
            selection3->setProperty("Alignment", 1);

            QAxObject *Tables = selection3->querySubObject("Tables()");
            auto range3 = doc->querySubObject("Range()");
            range3->dynamicCall("SetRange(int,int)", prob + 1, prob + 1);

            auto records = tableWidget->getText();
            QAxObject *table = Tables->querySubObject("Add(const QVariant&, const QVariant&, const QVariant&, "
                                                      "const QVariant& DefaultTableBehavior, const QVariant& AutoFitBehavior)",
                                                      range3->asVariant(), records.size() + 1, 3, 2, 1);
            table->querySubObject("Cell(Row, Column)", 1, 1)->querySubObject("Range()")
                    ->dynamicCall("InsertAfter(Text)", "№");
            table->querySubObject("Cell(Row, Column)", 1, 2)->querySubObject("Range()")
                    ->dynamicCall("InsertAfter(Text)", "наименование документа");
            table->querySubObject("Cell(Row, Column)", 1, 3)->querySubObject("Range()")
                    ->dynamicCall("InsertAfter(Text)", "№ страницы");
            QAxObject *cols = table->querySubObject("Columns()");
            cols->querySubObject("Item(int)", 1)->dynamicCall("Width", 55);
            cols->querySubObject("Item(int)", 2)->dynamicCall("Width", 350);
            cols->querySubObject("Item(int)", 3)->dynamicCall("Width", 70);
            int count = 0;
            std::for_each(std::execution::par, records.begin(), records.end(),
                          [&](const QPair<QString, QString>& record)
            {
                auto midRange = table->querySubObject("Cell(Row, Column)", count + 2, 2)->querySubObject("Range()");
                midRange->querySubObject("ParagraphFormat")->setProperty("Alignment", "wdAlignParagraphJustify");
                midRange->dynamicCall("InsertAfter(Text)", record.first);
                table->querySubObject("Cell(Row, Column)", count + 2, 1)
                        ->querySubObject("Range()")->dynamicCall("InsertAfter(Text)", QString::number(count + 1) + '.');
                table->querySubObject("Cell(Row, Column)", count++ + 2, 3)
                        ->querySubObject("Range()")->dynamicCall("InsertAfter(Text)", record.second);
            });
            selection3->dynamicCall("TypeParagraph()");
            word->setProperty("Visible", true);
            delete font;
            delete pswds;
            delete selection;
            delete font2;
            delete pswds2;
            delete selection2;
            delete font3;
            delete pswds3;
            delete al;
            delete table;
            delete Tables;
            delete doc;
            delete docs;
            delete word;
            process.waitForFinished();
        }
    }
}

