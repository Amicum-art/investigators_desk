#ifndef VIEWALLO_H
#define VIEWALLO_H
#include "factory_add_anything.h"


class ViewAllO :
        public FactoryEditMenu<QListView*>
{
    Q_OBJECT
public:
    explicit ViewAllO(QListView* listView = new QListView);
    static const QStringList getListOFromFile();
signals:
    void onLoad(QString);
};

#endif // VIEWALLO_H
