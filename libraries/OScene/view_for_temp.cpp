#include "view_for_temp.h"


ViewForTempl::ViewForTempl(QListView* listView) :
        FactoryEditMenu<QListView*>(listView)
{
    auto size = QGuiApplication::screens().constFirst()->availableGeometry();
    resize(size.width() * .44, size.height() - 40);
    setWindowTitle("Введите новый шаблон");
    setAttribute(Qt::WA_DeleteOnClose);
    listView->setModel(new QStringListModel(getTemplatesFromFile("wordlist.txt"), this));
    listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    auto font = listView->font();
    font.setPointSize(10);
    listView->setFont(font);
    connect(getOkBtn(), &QPushButton::clicked, [this, listView]() {
        auto lineEdit = new QLineEdit();
        addSingleName = std::make_unique<TemplateForInput>(lineEdit);
        connect(addSingleName.get(), &TemplateForInput::accepted, [this, lineEdit, listView]()
        {
            QString newWord = lineEdit->text();
            if (newWord.isEmpty())
                return;
            QFile file("wordlist.txt");
            if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
                QTextStream stream(&file);
                stream << newWord << '\n';
                file.close();
            }
            static_cast<QStringListModel*>(listView->model())
                    ->setStringList(getTemplatesFromFile("wordlist.txt"));
            emit updateModel();
        });
        addSingleName->show();
    });
    connect(getRemoveBtn(), &QPushButton::clicked, [this, listView]() {
        listView->model()->removeRow(listView->currentIndex().row());
        QFile file("wordlist.txt");
        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream stream(&file);
            for (auto it = 0; it < listView->model()->rowCount(); it++) {
                QString str = listView->model()->index(it, 0).data(Qt::DisplayRole).toString();
                stream << str << '\n';
            }
            file.close();
        }
        emit updateModel();
    });
    show();
}

const QStringList ViewForTempl::getTemplatesFromFile(const QString &fileName) {
    QFile file(fileName);
    QStringList words{};
    if (file.open(QFile::ReadOnly)){
        while (!file.atEnd()) {
            QString line = file.readLine();
            line.resize(line.length() - 1);
            if (!line.isEmpty())
                words << line;
        }
        file.close();
    }
    return words;
}
