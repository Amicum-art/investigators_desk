#ifndef INVENTORYSCENE_H
#define INVENTORYSCENE_H
#include "central_widget.h"
#include "my_table_widget.h"
#include "view_all_op.h"
#include "view_for_shap.h"
#include "view_for_temp.h"
#include "popup.h"


class Inventory_scene : public CentralWidget
{
    QHBoxLayout* mainLay;
    QVBoxLayout *centralLay;
    QHBoxLayout *layForTop;
    QLabel *label;
    QHBoxLayout *hForConsTop;
    QLabel * prevConsist;
    QComboBox *dOrM;
    QLineEdit *number_;
    MyTableWidget* tableWidget;
    QToolBar *toolbar;
    using descriptionTitle = AddingCustomWidgets<QLineEdit*>;
    std::unique_ptr<descriptionTitle> addTitle;

public:
    explicit Inventory_scene(QMainWindow *MainWindow);
    void saveIn();
    void loadO(QString name);
    void saveInWord() const;

};

#endif // INVENTORYSCENE_H
