#include "my_table_widget.h"


MyTableWidget::MyTableWidget(QWidget *parent)
    : QWidget{parent},
    vLay(new QVBoxLayout(this)),
    table(new QTableWidget(this)),
    addRow (new QPushButton(this)),
    addBefore(new QPushButton(this)),
    delRow(new QPushButton(this)),
    completer(new QCompleter(this))
{
    installEventFilter(this);
    const auto& styleF = "QPushButton { background-color: rgba(0,0,0,2);}"
                    "QToolTip { "
                        "color: #ffffff; "
                         "background-color: rgba(76, 74,72,0.6); "
                         "border: 1px solid white; "
                    "}"
                    "QPushButton:pressed {""background: #dadbde;}";
    table->setColumnCount(3);
    table->verticalHeader()->setHidden(true);
    table->horizontalHeader()->setHighlightSections(false);
    table->setHorizontalHeaderItem(0, new QTableWidgetItem("№"));
    table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    table->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeMode());
    table->setHorizontalHeaderItem(1, new QTableWidgetItem("наименование документа"));
    table->setHorizontalHeaderItem(2, new QTableWidgetItem("№ стр."));
    table->setStyleSheet("selection-color: rgba(0, 0, 213, 154);"
                               "font: 10pt \"MS Shell Dlg 2\";"
                               "border: 0;"
                               "selection-background-color: rgba(240, 240, 240, 1);");
    table->horizontalHeader()->setStyleSheet("QHeaderView::section { font: 8pt;"
                                                   "background-color:rgba(95, 95, 95, 225); }"
                                                   "color: rgba(95, 95, 95, 225);"
    );
    connect(table, &QTableWidget::currentCellChanged, this, &MyTableWidget::onCellChanged);
    completer->setModel(new QStringListModel(ViewForTempl::getTemplatesFromFile("wordlist.txt"), this));
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setFilterMode(Qt::MatchContains);
    completer->setWrapAround(false);
    vLay->addWidget(table);
    auto hLay = new QHBoxLayout();
    hLay->addWidget(addRow);
    addRow->setIcon(QIcon(QPixmap("qss/Op/add.png")));
    addRow->setIconSize({30, 30});
    addRow->setToolTip("Новая строка\nCtrl+R");
    addRow->setStyleSheet(styleF);
    addRow->setCursor(QCursor(Qt::PointingHandCursor));
    connect(addRow, &QPushButton::clicked, this, &MyTableWidget::onAddRow);
    onAddRow();
    hLay->addWidget(addBefore);
    addBefore->setIconSize({25, 25});
    addBefore->setStyleSheet(styleF);
    addBefore->setToolTip("Вставить строку над текущей");
    addBefore->setIcon(QIcon(QPixmap("qss/Op/before.png")));
    addBefore->setCursor(QCursor(Qt::PointingHandCursor));
    connect(addBefore, &QPushButton::clicked, this, &MyTableWidget::onAddBeforeRow);
    hLay->addWidget(delRow);
    delRow->setIconSize({25, 25});
    delRow->setToolTip("Удалить строку\nCtrl+D");
    delRow->setStyleSheet(styleF);
    delRow->setIcon(QIcon(QPixmap("qss/Op/dell.png")));
    delRow->setCursor(QCursor(Qt::PointingHandCursor));
    connect(delRow, &QPushButton::clicked, this, &MyTableWidget::onDelRow);
    vLay->addLayout(hLay);
    setLayout(vLay);
}

TextEdit *MyTableWidget::createItem()
{
    auto completingTextEdit = new TextEdit(table);
    completingTextEdit->setStyleSheet("color: rgb(0,0,0);");
    completingTextEdit->setCompleter(completer);
    QObject::connect(completingTextEdit, &TextEdit::textChanged, this, [this, completingTextEdit]() {
        auto normalH = completingTextEdit->toPlainText().length();
        table->setRowHeight(table->currentRow(), ((normalH < 65) ? 30 : 45));
    });
    return completingTextEdit;
}

void MyTableWidget::reCorrectly()
{
    static std::regex regex("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\-?(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)?$");
    if (auto w = qobject_cast<QLineEdit *>(table->cellWidget(table->currentRow(), 2))) {
        if (auto num_ = w->text();!std::regex_match(num_.toStdString(), regex)
                || (num_.length() > 3 && num_[3] != '-' && num_[2] != '-' && num_[1] != '-'))
        {
            num_.chop(1);
            w->setText(num_);
        } else
        {
            if (num_[num_.length() - 1] != '-')
                reNumerate();
        }
    }
}

void MyTableWidget::reNumerate(int i)
{
    auto curWidgetRow = (i == -1) ? table->currentRow() : i;
    if (curWidgetRow != -1) {
        if (curWidgetRow != table->rowCount() - 1) { //если строка не последняя
            auto *current = qobject_cast<QLineEdit *>(table->cellWidget(curWidgetRow, 2));
            auto tempCurrent = current->text();
            if (table->currentRow() == 0) {
                if (isSimple(tempCurrent)) {
                    current->setText("1"); // 121-12
                    tempCurrent = current->text();
                } else {
                    auto position = tempCurrent.indexOf('-');
                    int first = tempCurrent.first(position).toInt();
                    int second = tempCurrent.mid(position + 1).toInt();
                    if (first != 1) {
                        if (first < 1) first = 2;
                        int tmp = first - 1;
                        first = 1;
                        second -= tmp;
                        current->setText(QString::number(first) + '-' + QString::number(second));
                        tempCurrent = current->text().trimmed();
                    }
                }

            }
            int First, Second, Result;
            if (isSimple((tempCurrent))) {
                First = tempCurrent.toInt();
            } else {
                std::string temp = tempCurrent.toStdString();
                auto position = temp.find('-');
                First = std::stoi(temp.substr(position + 1));
            }
            if (auto *second = qobject_cast<QLineEdit *>(table->cellWidget(curWidgetRow + 1, 2))) {
                auto tempSecond = second->text();
                if (isSimple(tempSecond)) {
                    Second = tempSecond.toInt();
                } else {
                    auto position = tempSecond.indexOf('-');
                    Second = tempSecond.first(position).toInt();
                }
                Result = First - Second + 1;
                for (int i = curWidgetRow + 1; i < table->rowCount(); i++) {
                    if (auto *p = qobject_cast<QLineEdit *>(table->cellWidget(i, 2))) {
                        auto tempRes = p->text();
                        if (isSimple(tempRes)) {
                            tempRes = QString::number(tempRes.toInt() + Result);
                        } else {
                            auto position = tempRes.indexOf('-');
                            tempRes = QString::number(tempRes.first(position).toInt() + Result) + '-'
                                   + QString::number(tempRes.mid(position + 1).toInt() + Result);
                        }
                        p->setText(tempRes);
                    }
                }
            }
        }
    }
}

bool MyTableWidget::isSimple(const QString &str)
{
    for (const auto &sym: str) {
        if ((sym > '9' || sym < '0')) {
            return false;
        }
    }
    return true;
}

bool MyTableWidget::eventFilter(QObject *object, QEvent *event) {
    if (event->type() == QEvent::KeyPress) {
        auto *keyEvent = dynamic_cast<QKeyEvent *>(event);
        if ((keyEvent->modifiers() & Qt::ControlModifier) && keyEvent->key() == Qt::Key_R) {
            addRow->click();
            return true;
        }
        if ((keyEvent->modifiers() & Qt::ControlModifier) && keyEvent->key() == Qt::Key_D) {
            delRow->click();
            return true;
        }
        if ((keyEvent->modifiers() & Qt::ControlModifier) && keyEvent->key() == Qt::Key_Tab) {
            table->nextInFocusChain();
            return true;
        }
    }
    return QWidget::eventFilter(object, event);
}

void MyTableWidget::resizeEvent(QResizeEvent *) {
    table->setColumnWidth(0, width() * .045);
    table->setColumnWidth(1, width() * .80 - 35);
    table->setColumnWidth(2, width() * .11+15);
    updateGeometry();
}

void MyTableWidget::onAddRow()
{
    emit table->itemSelectionChanged();
    int i = table->rowCount() + 1;
    table->setRowCount(i);
    auto firstItem = new QTableWidgetItem(QString::number(i) + '.');
    firstItem->setFlags(firstItem->flags() ^ Qt::ItemIsEditable);
    firstItem->setForeground(QBrush(QColor(0, 0, 0)));
    table->setItem(i - 1, 0, firstItem);
    auto widget = createItem();
    table->setCellWidget(i - 1, 1, widget);
    widget->setFocus();
    QString num = "1";
    if (i != 1) {
        num = qobject_cast<QLineEdit *>(table->cellWidget(table->currentRow() - 1, 2))->text();
        if (isSimple(num)) {
            num = QString::number(num.toInt() + 1);
        } else {
            auto position = num.indexOf('-');
            num = QString::number(num.mid(position + 1).toInt() + 1);
        }
    }
    auto numbers = new QLineEdit(num, table);
    numbers->setStyleSheet("color:rgb(0,0,0);");
    table->setCellWidget(i - 1, 2, numbers);
    connect(numbers, &QLineEdit::textChanged, this, &MyTableWidget::reCorrectly);
}

void MyTableWidget::onDelRow()
{
    auto current = table->currentRow();
    if (current != -1) {
        table->setCurrentItem(
                dynamic_cast<QTableWidgetItem *>(table->cellWidget(table->currentRow(), 0)));
        table->removeRow(current);
        reNumerate(current - 1);
        for (int i = 0; i < table->rowCount(); i++) {
            auto item = table->item(i, 0);
            item->setText(QString::number(i + 1) + '.');
        }
        if (!current && table->rowCount() != 0)
            table->cellWidget(0, 1)->setFocus();
    }
}

void MyTableWidget::onAddBeforeRow()
{
    emit table->itemSelectionChanged();
    if (table->currentRow() != -1) {
        auto before = table->currentRow();
        table->setRowCount(table->rowCount() + 1);
        auto p = new QTableWidgetItem(QString::number(table->rowCount()) + '.');
        p->setForeground(QBrush(QColor(0, 0, 0)));
        table->setItem(table->rowCount() - 1, 0, p);
        TextEdit *tempC = createItem();
        QString tempF;
        if (before != 0) {
            tempF = qobject_cast<QLineEdit *>(table->cellWidget(table->currentRow() - 1, 2))->text();
            if (isSimple(tempF)) {
                tempF = QString::number(tempF.toInt() + 1);
            } else {
                auto position = tempF.indexOf('-');
                tempF = QString::number(tempF.mid(position + 1).toInt() + 1);
            }
        }
        for (int i = before; i < table->rowCount() - 1; i++) {
            if (i != before) {
                auto current = createItem();
                if (auto *p = qobject_cast<TextEdit *>(table->cellWidget(i, 1))) {
                    current->setText(p->toPlainText());
                }
                table->setCellWidget(i, 1, tempC);
                tempC = current;
            } else {
                auto item = createItem();
                if (auto *p = qobject_cast<TextEdit *>(table->cellWidget(i, 1))) {
                    tempC->setText(p->toPlainText());
                }
                table->setCellWidget(i, 1, item);
            }
            if (auto *p = qobject_cast<QLineEdit *>(table->cellWidget(i, 2))) {
                if (i != 0) {
                    auto numbers = new QLineEdit(tempF, table);
                    numbers->setStyleSheet("color:rgb(0,0,0);");
                    table->setCellWidget(i, 2, numbers);
                    connect(numbers, &QLineEdit::textChanged, this, &MyTableWidget::reCorrectly);
                    tempF = p->text();
                } else {
                    tempF = p->text();
                    auto numbers = new QLineEdit("1", table);
                    numbers->setStyleSheet("color:rgb(0,0,0);");
                    table->setCellWidget(i, 2, numbers);
                    connect(numbers, &QLineEdit::textChanged, this, &MyTableWidget::reCorrectly);
                }
                if (isSimple(tempF)) {
                    tempF = QString::number(tempF.toInt() + 1);
                } else {
                    auto position = tempF.indexOf('-');
                    tempF = QString::number(tempF.first(position).toInt() + 1) + '-'
                           + QString::number(tempF.mid(position + 1).toInt() + 1);
                }

            }
        }
        table->setCellWidget(table->rowCount() - 1, 1, tempC);
        auto numbers = new QLineEdit(tempF, table);
        numbers->setStyleSheet("color:rgb(0,0,0);");
        table->setCellWidget(table->rowCount() - 1, 2, numbers);
        connect(numbers, &QLineEdit::textChanged, this, &MyTableWidget::reCorrectly);
    }
}

void MyTableWidget::setNewText(QVector<QStringList> vec)
{
    table->setRowCount(vec.size());
    //emit table->itemSelectionChanged();
    for(int i = 0;const auto& item: vec) {
        auto firstItem = new QTableWidgetItem(QString::number(++i) + '.');
        firstItem->setForeground(QBrush(QColor(0, 0, 0)));
        table->setItem(i - 1, 0, firstItem);
        auto widget = createItem();
        widget->setText(item.first());
        table->setCellWidget(i - 1, 1, widget);
        table->setRowHeight(table->currentRow(), 45);
        widget->setFocus();
        auto numbers = new QLineEdit(item.last(), table);
        numbers->setStyleSheet("color:rgb(0,0,0);");
        table->setCellWidget(i - 1, 2, numbers);
        connect(numbers, &QLineEdit::textChanged, this, &MyTableWidget::reCorrectly);
    }
}

QVector<QPair<QString,QString>> MyTableWidget::getText()
{
    QVector<QPair<QString,QString>> vec;
    for (int i = 0; i < table->rowCount(); i++)
    {
        if (auto *p = qobject_cast<TextEdit *>(table->cellWidget(i, 1)))
            if (auto *pp = qobject_cast<QLineEdit *>(table->cellWidget(i, 2)))
                vec.emplace_back(p->toPlainText(), pp->text());
    }
    return vec;
}

void MyTableWidget::updadeCompleter()
{
    static_cast<QStringListModel*>(completer->model())
            ->setStringList(ViewForTempl::getTemplatesFromFile("wordlist.txt"));
}

void MyTableWidget::onCellChanged(int currRow, int currColumn,
                                  int prevRow [[maybe_unused]], int prevColumn [[maybe_unused]])
{
    if (auto line = qobject_cast<QLineEdit *>(table->cellWidget(currRow, currColumn))) {
        auto number = line->text();
        if (number.isEmpty()) {
            if (!currRow) {
                number = "1";
            } else {
                if (auto preWgt = qobject_cast<QLineEdit *>(table->cellWidget(currRow - 1, currColumn))) {
                    auto preline = preWgt->text();
                    if (isSimple(preline)) {
                        number = QString::number(preline.toInt() + 1);
                    } else {
                        auto position = preline.indexOf('-');
                        number = QString::number(preline.mid(position + 1).toInt() + 1);
                    }
                }
            }
        } else if (number[number.length() - 1] == '-') {
            number.chop(1);
        }
        line->setText(number);
    }
    reNumerate();
}
