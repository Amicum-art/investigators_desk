#ifndef VIEWFORSHAP_H
#define VIEWFORSHAP_H
#include "factory_add_anything.h"

class ViewForShap :
        public AddingCustomWidgets<QTextEdit *, QLineEdit*> {
public:
    explicit ViewForShap(QTextEdit * bigText = new QTextEdit,
                         QLineEdit* name = new QLineEdit);
    static const QStringList getShapFromFile();
};

#endif // VIEWFORSHAP_H
