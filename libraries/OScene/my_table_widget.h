#ifndef MYTABLEWIDGET_H
#define MYTABLEWIDGET_H
#include "text_edit.h"
#include "view_for_temp.h"

class MyTableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MyTableWidget(QWidget *parent = nullptr);
private:
    TextEdit *createItem();
    void reCorrectly();
    void reNumerate(int i = -1);
    static bool isSimple(const QString &str);
protected:
    bool eventFilter(QObject *object, QEvent *event) override;
    void resizeEvent(QResizeEvent *) override;
public slots:
    void onAddRow();
    void onDelRow();
    void onAddBeforeRow();
    void setNewText(QVector<QStringList> vec);
    QVector<QPair<QString,QString>> getText();
    void updadeCompleter();
    void onCellChanged(int currentRow, int currentColumn, int prevRow, int prevColumn);
private:
    QVBoxLayout *vLay;
    QTableWidget* table;
    QPushButton *addRow;
    QPushButton* addBefore;
    QPushButton* delRow;
    QCompleter *completer = nullptr;
};

#endif // MYTABLEWIDGET_H
