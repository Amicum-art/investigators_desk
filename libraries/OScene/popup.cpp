#include "popup.h"



PopUp::PopUp(QWidget *parent) : QPushButton(parent) {
    setWindowFlags(Qt::FramelessWindowHint |        // Отключаем оформление окна
                   Qt::Tool |                       // Отменяем показ в качестве отдельного окна
                   Qt::WindowStaysOnTopHint);       // Устанавливаем поверх всех окон
    setAttribute(Qt::WA_TranslucentBackground);     // Указываем, что фон будет прозрачным
    setAttribute(Qt::WA_ShowWithoutActivating);     // При показе, виджет не получается фокуса автоматически

    animation.setTargetObject(this);                // Устанавливаем целевой объект анимации
    animation.setPropertyName("popupOpacity");      // Устанавилваем анимируемое свойство
    //                /* Подключаем сигнал окончания
    //                        * анимации к слоты скрытия
    //                          * */

    // Настройка текста уведомления
    label.setAlignment(Qt::AlignHCenter | Qt::AlignVCenter); // Устанавилваем по центру
    // И настраиваем стили
    label.setStyleSheet("QLabel { "
                        "color : white; "
                        "margin-top: 6px;"
                        "margin-bottom: 6px;"
                        "margin-left: 10px;"
                        "margin-right: 10px; }");
    auto font = label.font();
    font.setPointSize(13);
    label.setFont(font);
    // Производим установку текста в размещение, ...
    layout.addWidget(&label, 0, 0);
    setLayout(&layout); // которое помещаем в виджет

    // По сигналу таймера будет произведено скрытие уведомления, если оно видимо
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &PopUp::hideAnimation);
}

void PopUp::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event)

    /* А теперь настраиваем фон уведомления,
     * который является прямоугольником с чёрным фоном
     * */
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing); // Включаем сглаживание

    // Подготавливаем фон. rect() возвращает внутреннюю геометрию виджета уведомления, по содержимому
    QRect roundedRect;
    roundedRect.setX(rect().x() + 5);
    roundedRect.setY(rect().y() + 5);
    roundedRect.setWidth(rect().width() - 10);
    roundedRect.setHeight(rect().height() - 10);

    // Кисть настраиваем на чёрный цвет в режиме полупрозрачности 180 из 255
    painter.setBrush(QBrush(QColor(0, 0, 0, 180)));
    painter.setPen(Qt::NoPen); // Край уведомления не будет выделен

    // Отрисовываем фон с закруглением краёв в 10px
    painter.drawRoundedRect(roundedRect, 10, 10);
}

void PopUp::setPopupText(const QString &text) {
    label.setText(text);    // Устанавилваем текст в Label
    adjustSize();           // С пересчётом размеров уведомления
}

void PopUp::show() {
    setWindowOpacity(0.0);  // Устанавливаем прозрачность в ноль

    animation.setDuration(4000);     // Настравиваем длительность анимации
    animation.setStartValue(0.0);   // Стартовое значение будет 0 (полностью прозрачный виджет)
    animation.setEndValue(1.0);     // Конечное - полностью непрозрачный виджет

    auto size = QGuiApplication::screens().constFirst()->availableGeometry();
    setGeometry(size.width() - 36 - width() + size.x(),
                size.height() - 36 - height() + size.y(),
                width(),
                height());
    QWidget::show();                // Отображаем виджет, который полностью прозрачен

    animation.start();              // И запускаем анимацию
    timer->start(100);             // А также стартует таймер, который запустит скрытие уведомления через 3 секунды
}

void PopUp::hideAnimation() {
    timer->stop();
    connect(this, &PopUp::clicked, [this]() {
        animation.setDuration(1500);    // Настравиваем длительность анимации
        animation.setStartValue(1.0);   // Стартовое значение будет 1 (полностью непрозрачный виджет)
        animation.setEndValue(0.0);     // Конечное - полностью прозрачный виджет
        animation.start();
        connect(&animation, &QAbstractAnimation::finished, this, &PopUp::hide);
        // И запускаем анимацию
    });
    // Останавлвиваем таймер

}

void PopUp::hide() {
    // Если виджет прозрачный, то скрываем его
    delete this;
}

void PopUp::setPopupOpacity(float opacity) {
    popupOpacity = opacity;

    setWindowOpacity(opacity);
}

float PopUp::getPopupOpacity() const {
    return popupOpacity;
}

