#include "view_for_shap.h"
#include "crypt.h"
ViewForShap::ViewForShap(QTextEdit * bigText, QLineEdit* name) :
    AddingCustomWidgets<QTextEdit *, QLineEdit*>(bigText, name)
{
    auto size = QGuiApplication::screens().constFirst()->availableGeometry();
    setFixedSize(size.width() * .33, size.height() * .21);
    setWindowTitle("Введите нижнюю часть описи");
    setAttribute(Qt::WA_DeleteOnClose);
    bigText->setMinimumWidth(size.width() * .22);
    const auto& shap = getShapFromFile();
    if(!shap.isEmpty()){
        bigText->setText(shap[0]);
        name->setText(shap[1]);
    }
    connect(this, &QDialog::accepted, [this, bigText, name]() {
        QString res = bigText->toPlainText() + '&' + name->text();
        res = Code::crypt(QStringRef(&res));
        QFile file("list.txt");
        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)) {
            QTextStream stream(&file);
            stream << res;
            file.close();
        }
    });
}

const QStringList ViewForShap::getShapFromFile()
{
    QFile file("list.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QString lines = file.readAll();
        lines = Code::deCrypt(QStringRef(&lines));
        file.close();
        return lines.split('&');
    } else {
        return {};
    }
}
