#ifndef VIEWFORTEMPLATE_H
#define VIEWFORTEMPLATE_H
#include "factory_add_anything.h"

class ViewForTempl :
        public FactoryEditMenu<QListView*> {
    Q_OBJECT
public:
    explicit ViewForTempl(QListView* listView = new QListView);
    static const QStringList getTemplatesFromFile(const QString &fileName);
private:
    using TemplateForInput = AddingCustomWidgets<QLineEdit*>;
    std::unique_ptr<TemplateForInput> addSingleName;
signals:
    void updateModel();
};

#endif // VIEWFORTEMPLATE_H
