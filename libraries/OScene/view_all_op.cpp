#include "view_all_op.h"
#include "crypt.h"

ViewAllO::ViewAllO(QListView* listView):
    FactoryEditMenu<QListView*>(listView)
{
    auto size = QGuiApplication::screens().constFirst()->availableGeometry();
    resize(size.width() * .185, size.height() * .21);
    setAttribute(Qt::WA_DeleteOnClose);
    listView->setModel(new QStringListModel(getListOFromFile(), this));
    listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    auto font = listView->font();
    font.setPointSize(10);
    listView->setFont(font);
    getOkBtn()->setText("Загрузить опись");
    connect(getOkBtn(), &QPushButton::clicked, this, [this, listView]() {
        if (listView->currentIndex().row() != -1){
            emit onLoad(Code::crypt(listView->model()
                    ->index(listView->currentIndex().row(), 0).data(Qt::DisplayRole).toString()));
            close();
        }
    });
    connect(getRemoveBtn(), &QPushButton::clicked, [this, listView]() {
        if (listView->currentIndex().row() != -1) {
            QFile::remove(QString("systemO") + "/"
                          + Code::crypt(listView->model()->index(listView->currentIndex().row(), 0).
                    data(Qt::DisplayRole).toString()) + ".txt");
            listView->model()->removeRow(listView->currentIndex().row());
        }
    });
    show();
}

const QStringList ViewAllO::getListOFromFile()
{
    QDir dir(QString::fromStdString("systemO"));
    dir.setNameFilters(QStringList("*.txt"));
    dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    QStringList fileList = dir.entryList();
    for (int i = 0; i < fileList.size(); i++) {
        fileList[i] = Code::deCrypt(QStringRef (&fileList[i], 0, fileList[i].length() - 4));
    }
    return fileList;
}
