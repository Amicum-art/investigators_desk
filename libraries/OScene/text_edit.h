#ifndef TEXTEDIT_H
#define TEXTEDIT_H



class TextEdit : public QTextEdit {
Q_OBJECT
public:
    TextEdit(QWidget *parent = nullptr);
    ~TextEdit() = default;
    void setCompleter(QCompleter *c);
    QCompleter *completer() const;
protected:
    void keyPressEvent(QKeyEvent *e) override;
    void focusInEvent(QFocusEvent *e) override;
private slots:
    void insertCompletion(const QString &completion);
private:
    QString textUnderCursor() const;
private:
    QCompleter *c = nullptr;
};

#endif // TEXTEDIT_H
