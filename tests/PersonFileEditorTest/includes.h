#ifndef INCLUDES_H
#define INCLUDES_H


#include <QLineEdit>
#include <QLabel>
#include <QTableWidget>
#include <QGuiApplication>
#include <QApplication>
#include <QScreen>
#include <QHeaderView>
#include <QComboBox>
#include <QListView>
#include <QDir>
#include <QFileInfo>
#include <QWidget>
#include <QDate>
#include <QDateTime>
#include <QDateEdit>
#include <QDateTimeEdit>
#include <QTableWidget>
#include <QMessageBox>
#include <QFile>
#include <optional>
#include <string>
#include <QString>
#include <QObject>
#include <QTextEdit>
#include <memory>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QGridLayout>
#include <QRadioButton>
#include <QTextStream>
#include <QStringRef>
#include <QStringListModel>
#include <QStringList>
#include <type_traits>
#include <QDialog>
#include <concepts>
#include <tuple>
#include <variant>
#include <QClipboard>
#include <QFontDatabase>
#include <QMainWindow>
#include <QAbstractItemView>
#include <QKeyEvent>
#include <QScrollBar>
#include <QCompleter>
#include <QToolButton>
#include <execution>
#include <ranges>
#include <regex>
#include <Windows.h>


#endif // INCLUDES_H
