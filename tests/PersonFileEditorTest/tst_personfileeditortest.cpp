#include <QtTest>
#include "person_edit.h"
#include "crypt.h"

class PersonFileEditorTest : public QObject {
    Q_OBJECT
private slots:
    void initTestCase() {
        Code::initMap();
        Code::key = "secret key";
    }
    void cleanupTestCase()
    {

    }
    void testAddPerson() {
        // Test adding a person with a valid file name
        PersonFileEditor editor;
        editor.addPerson(".", "person");
        QVERIFY(QFile::exists("./" + Code::crypt(QString("person")) + ".txt"));

        // Test adding a person with an empty file name
        editor.addPerson(".", "");
        QVERIFY(!QFile::exists("./" + Code::crypt(QString("")) + ".txt"));
    }

    void testRenamePerson() {

        PersonFileEditor editor;
        editor.renamePerson(".", "person", "newName");
        QVERIFY(QFile::exists("./" + Code::crypt(QString("newName")) + ".txt"));
        QVERIFY(!QFile::exists("./" + Code::crypt(QString("person")) + ".txt"));

        // Test renaming the file with an empty new name
        editor.renamePerson(".", "newName", "n");
        QVERIFY(!QFile::exists("./" + Code::crypt(QString("newName")) + ".txt"));
        QVERIFY(QFile::exists("./" + Code::crypt(QString("n")) + ".txt"));
    }

    void testReplacePerson() {
        // Create a file with an old directory and name

        QFile file("./" + Code::crypt(QString("fileName")) + ".txt");
        file.open(QIODevice::WriteOnly);
        file.close();

        // Test replacing the file with a new directory
        PersonFileEditor editor;
        editor.replasePerson("./", "newDir", "fileName");
        QVERIFY(QFile::exists("newDir/" + Code::crypt(QString("fileName")) + ".txt"));
        QVERIFY(!QFile::exists("./" + Code::crypt(QString("fileName")) + ".txt"));
    }

    void testDeletePerson() {
        // Test deleting the file
        PersonFileEditor editor;
        editor.deletePerson("newDir/", "fileName");
        QVERIFY(!QFile::exists("newDir/" + Code::crypt(QString("fileName")) + ".txt"));
        editor.deletePerson(".", QString("n"));
        QVERIFY(!QFile::exists("./" + Code::crypt(QString("n")) + ".txt"));
    }

    void testGetCasesList() {
        // Test getting a list of cases
        PersonFileEditor editor;
        auto cases = editor.getCasesList();
        QVERIFY(cases.has_value());
        QCOMPARE(cases.value().size(), 0);
    }
};


QTEST_APPLESS_MAIN(PersonFileEditorTest)

#include "tst_personfileeditortest.moc"
