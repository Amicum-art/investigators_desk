#include <QtTest>
#include "crypt.h"

class CodeTest : public QObject {
    Q_OBJECT
private slots:
    void initTestCase() {
        Code::initMap();
        Code::key = "secret key";
    }

    void testCrypt() {
        QString input = "hello world";
        QString output = Code::crypt(input);
        QVERIFY2(output != input, "Output should be different from input.");
        QString decoded = Code::deCrypt(output);
        QCOMPARE(decoded, input);
    }

    void testCryptWithEmptyString() {
        QString input = "";
        QString output = Code::crypt(input);
        QCOMPARE(output, input);
        QString decoded = Code::deCrypt(output);
        QCOMPARE(decoded, input);
    }

    void testCryptWithUnicodeCharacters() {
        QString input = u8"你好，世界！";
        QString output = Code::crypt(input);
        QVERIFY2(output != input, "Output should be different from input.");
        QString decoded = Code::deCrypt(output);
        QCOMPARE(decoded, input);
    }

    void testCryptWithLongString() {
        QString input (1000, 'a');
        QString output = Code::crypt(input);
        QVERIFY2(output != input, "Output should be different from input.");
        QString decoded = Code::deCrypt(output);
        QCOMPARE(decoded, input);
    }
};


QTEST_APPLESS_MAIN(CodeTest)

#include "tst_codetest.moc"
