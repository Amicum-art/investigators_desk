#include <QtTest>
#include <memory>
#include <QCoreApplication>
#include "dictionary_converter.h"
#include "instrumentor.h"

class TesterConverter : public QObject
{
    Q_OBJECT
    std::unique_ptr<DictionaryConverter> converter;
    QString key, value;
public:
    TesterConverter();
    ~TesterConverter();;
private slots:
    void convertMan();
    void convertWomaw();
    void convertOther();
    void convertPhrase();
    void simpleConversion();
    void insertAndRemove();
};

TesterConverter::TesterConverter():
    converter(std::make_unique<DictionaryConverter>()),
    key("собака"), value("друг")
{
    Instrumentor::Get().BeginSession("Converter");
}

TesterConverter::~TesterConverter()
{
    Instrumentor::Get().EndSession();
}

void TesterConverter::convertMan()
{
    PROFILE_FUNCTION()
    QVERIFY2(converter->convert(true, "Про меня !Подо мною. Подо мной")
             == "Про него !Под ним. Под ним",
             "Prepositions and compound words");
    QVERIFY2(converter->convert(true, "я, меня, обо мне, обо мне.")
             == "он, его, о нём, о нём.", "Pronouns");
}

void TesterConverter::convertWomaw()
{
    PROFILE_FUNCTION()
    QVERIFY2(converter->convert(false, "Про меня !Подо мною. Подо мной")
             == "Про неё !Под ней. Под ней",
             "Prepositions and compound words");
    QVERIFY2(converter->convert(false, "я, меня, обо мне, обо мне.")
             == "она, её, о ней, о ней.", "Pronouns");
}

void TesterConverter::convertOther()
{
    PROFILE_FUNCTION()
    QVERIFY2(converter->convert(false, "ловлю, краду , признаюсь, смеюсь")
             == "ловит, крадёт , признаётся, смеётся", "Verbs");
    QVERIFY2(converter->convert(false, "от нас, крот наступил")
                 == "от них, крот наступил", "Substitution word in other words");
}

void TesterConverter::convertPhrase()
{
    PROFILE_FUNCTION()
    bool isMan = true;
    QString str = "ко мне, обо мне, про меня, про нас";
    converter->phraseConversion(isMan, str);
    QVERIFY2(str == "к нему, о нём, про него, про них", "Сompound male pronoun conversion");
    str = "ко мне, обо мне, про меня, про нас";
    converter->phraseConversion(!isMan, str);
    QVERIFY2(str == "к ней, о ней, про неё, про них", "Сompound feminine pronoun conversion");
}

void TesterConverter::simpleConversion()
{
    PROFILE_FUNCTION()
    bool isMan = true;
    QString str = "я моей сестре мою игрушку, мною сделанную, дарю";
    QVERIFY2(converter->subsequentConversion(isMan, str) == "он его сестре его игрушку, им сделанную, дарит",
            "transformation of simple verbs and pronouns into masculine gender");
    QVERIFY2(converter->subsequentConversion(!isMan, str) == "она её сестре её игрушку, ей сделанную, дарит",
             "transformation of simple verbs and pronouns into feminine");
}

void TesterConverter::insertAndRemove()
{
    PROFILE_FUNCTION()
    QString word {"собака"};
    QVERIFY2(converter->convert(true, word) == word,
            "Checking for the absence of a word");
    Dictionary::insertToDictionary(key, value);
    QVERIFY2(converter->convert(true, word) == "друг",
            "Inserted word conversion");
    Dictionary::removeFromDictionary(key);
    QVERIFY2(converter->convert(true, word) == word,
            "Deleted word conversion");
}


QTEST_MAIN(TesterConverter)

#include "tst_testerconverter.moc"
