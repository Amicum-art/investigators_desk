#!/bin/bash
set -e
exec &> log.txt
 
TAG_NAME="v${COMMIT_DATETIME}"
TAG_MESSAGE="Version created on ${COMMIT_DATETIME}"
FILE_NAME="./Deploy/Amicum.exe"

# Проверяем, существует ли тег с таким именем
if git rev-parse "${TAG_NAME}" >/dev/null 2>&1; then
    echo "Tag ${TAG_NAME} already exists"
else
    git tag -a "${TAG_NAME}" -m "${TAG_MESSAGE}" --annotate -m "Created by ${GIT_USER_NAME} <${GIT_USER_EMAIL}>"
    echo "Created tag ${TAG_NAME}"
    git push --tags git@gitlab.com:Amicum-art/investigators_desk.git
fi

# создание новой загрузки в проекте и получение markdown-текста для использования в release notes
RESPONSE=$(curl --request POST --header "PRIVATE-TOKEN: ${CI_PRIVATE_TOKEN}" --form "file=@./Deploy/Amicum.exe" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/uploads")
MARKDOWN=$(echo ${RESPONSE} | jq -r '.markdown')
echo $MARKDOWN

RESPONSE=$(curl --header "PRIVATE-TOKEN: ${CI_PRIVATE_TOKEN}" --request POST "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases" \
--form "tag_name=${TAG_NAME}" \
--form "description=${MARKDOWN}" \
--form "ref=main")

echo ${RESPONSE}
