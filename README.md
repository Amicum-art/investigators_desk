# Amicum-application
##### The Amicum software package contains a number of functional tools to reduce labor costs in the production of a preliminary investigation
[![N|Solid](https://sun9-2.userapi.com/impg/-DptbO4efJGu6Uvg2NZ4AEI-sgGDwDZOELUxAg/UdigloY0aRA.jpg?size=256x256&quality=95&sign=5d1305e5f8b4f1754106371ac3bdad9e&type=album)](https://gitlab.com/Amicum-art)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
# Содержание
* [Functionalities](#The application has a number of functionalities:)
* [Quote of the Day](#Quote of the Day)
* [Tech](#Tech)
* [Installation](#Installation)
* [License](#License)
* [Release](#Release of this program:)

### <a name="The application has a number of functionalities:">The application has a number of functionalities:</a> 

- The main tray of the program is equipped with a converter of readings from the first person to the third, depending on the gender of the person
- The menu is represented by sections of criminal cases and verification materials. By adding criminal cases and verification materials, they are generally stored, timing records of the investigative actions performed.
- Inventory creation tools allow you to maintain a saved list of inventories, upload to Word and create your own templates. Templates, incl. set personally by the user, are used as the output of auto-completion of the typed document name.
- Automatic numbering recalculates pages when new documents are added, regardless of their location: at the beginning or in another part of the inventory.
- The built-in calendar has the purpose of navigation when calculating the terms of extension for 10 and 30 days from a hidden tray, as well as viewing plans for the day for all relevant cases and materials, if any, for the selected date.
- It is possible to upload timekeeping to other documents and programs.
- ✨Now everything is easier

### <a name="Quote of the Day">Quote of the Day</a>
> "Work should bring not only material benefits,  but also spiritual satisfaction. After all, we do not live for the sake of work, but work for the sake of life." - Confucius.

### <a name="Tech">Tech</a>
Amicum uses a number of open source projects to work properly:

- [C++]() - is an object-oriented programming language that is used to develop applications at the level of system programming, scientific and engineering calculations, computer statistics, and web applications.
- [Qt]() - is a cross-platform C++ development framework. Qt provides a set of tools for developing graphical user interfaces, networking, databases, multimedia, and many other applications.
- [Testing Qt]() - is a tool for discovering touch interfaces and more. It allows you to create test scripts, automate their launch and receive test execution reports.
- [CMake]() - is a cross-platform system for building C++ projects. It allows you to automate the process of building a project, determining dependencies, and installing the project on the user's computer. CMake allows you to create projects for various system and compiler projects.
- [Inno Setup]() - is a free tool for creating application installers on the Windows platform. It allows you to automate the process of installing software on a user's computer.

And of course Amicum itself is open source with a [public repository][dill]
 on GitLab.

### <a name="Installation">Installation</a>

Install Qt framework library, compiler(MinGW64/MSVC...or other build system), CMake.
Next, download the source code for the project and run a series of commands on the command line from the project folder.
Version for MinGW64:
```sh
mkdir build 
cd build
cmake -G "MinGW Makefiles" ..
mingw32-make
```

For production environments...

```sh
mkdir build
cd build
cmake --build . --target install
```
### <a name="Release of this program:">Release of this program:</a>
[Amicum.exe](/uploads/fe2060fa4a1b9e396176bc9592ee3faf/Amicum.exe)

### <a name="License">License</a>

**Free Software, Hell Yeah!**
